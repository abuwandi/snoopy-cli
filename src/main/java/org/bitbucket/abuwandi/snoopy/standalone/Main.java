package org.bitbucket.abuwandi.snoopy.standalone;

import java.util.concurrent.atomic.AtomicReference;
import org.bitbucket.abuwandi.snoopy.standalone.execution.Execution;
import org.bitbucket.abuwandi.snoopy.standalone.execution.Executions;

/** The type Main. */
public class Main {

  /**
   * The entry point of application.
   *
   * @param args the input arguments
   */
  public static void main(String[] args) {
    final AtomicReference<Throwable> error = new AtomicReference<>(null);
    try {
      final Execution execution = Executions.execution(args);
      Runtime.getRuntime().addShutdownHook(new Thread(() -> execution.shutdown(error.get())));
      execution.execute();
      System.exit(0);
    } catch (Throwable throwable) {
      error.compareAndSet(null, throwable);
      System.exit(1);
    }
  }
}
