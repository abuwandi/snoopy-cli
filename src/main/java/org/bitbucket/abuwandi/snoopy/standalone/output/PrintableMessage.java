package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Setter;

/** The type Printable message. */
@Setter
public class PrintableMessage implements Printable {

  private final String message;
  private final int line;
  private final ConsoleColor color;
  private final int linesReserved;

  @Setter(AccessLevel.NONE)
  private final AtomicBoolean printed;

  /**
   * Instantiates a new Printable message.
   *
   * @param line the line
   * @param message the message
   * @param color the color
   * @param linesReserved the lines reserved
   * @param printed the printed
   */
  @Builder
  public PrintableMessage(
      final int line,
      final String message,
      final ConsoleColor color,
      final int linesReserved,
      final boolean printed) {
    this.line = line;
    this.message = message;
    this.color = Optional.ofNullable(color).orElse(ConsoleColor.DEFAULT);
    this.linesReserved = Math.max(linesReserved, 1);
    this.printed = new AtomicBoolean(printed);
  }

  @Override
  public int line() {
    return line;
  }

  @Override
  public String print() {
    return message;
  }

  @Override
  public ConsoleColor color() {
    return color;
  }

  @Override
  public int linesReserved() {
    return linesReserved;
  }

  @Override
  public boolean isPrinted() {
    return this.printed.get();
  }

  @Override
  public void setPrinted(final boolean printed) {
    this.printed.set(printed);
  }
}
