package org.bitbucket.abuwandi.snoopy.standalone.execution;

import io.netty.handler.codec.http.HttpHeaderNames;
import io.reactivex.rxjava3.core.Single;
import java.net.URI;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.Snoopy;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpResponse;
import org.bitbucket.abuwandi.snoopy.client.http.authentication.OAuth2;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.OutputMode;
import org.bitbucket.abuwandi.snoopy.standalone.output.RichConsole;
import org.bitbucket.abuwandi.snoopy.standalone.util.HttpUtils;
import org.bitbucket.abuwandi.snoopy.standalone.util.Validators;

/** The type Executions. */
public class Executions {

  /**
   * Execution execution.
   *
   * @param args the args
   * @return the execution
   */
  public static Execution execution(String... args) {

    final Console console = new RichConsole();

    try {
      if (args == null || args.length < 1) {
        throw new IllegalArgumentException(
            "No arguments passed, I am guessing you want some help ...");
      }

      final URI uri = Validators.validateUri(args[args.length - 1]);

      if (Objects.isNull(uri)) {
        throw new IllegalArgumentException("Missing a valid url");
      }

      final Map<String, ArgumentValue<?>> arguments = parseArguments(args);

      checkArgumentDependencies(arguments);

      final ArgumentValue<?> executionModeArg = arguments.get(Argument.EXECUTION_MODE.value());

      if (executionModeArg.isDefaultValue() && isYouTube(uri)) {
        overrideYouTubeExecutionDefaults(arguments);
      } else if (executionModeArg.isDefaultValue() && isFileUpload(arguments)) {
        overrideUploadExecutionDefaults(arguments);
      } else if (executionModeArg.isDefaultValue() && isFileDownload(console, uri, arguments)) {
        overrideAcceleratedGetDefaults(arguments);
      }

      final ExecutionMode executionMode =
          (ExecutionMode) arguments.get(Argument.EXECUTION_MODE.value()).getValue();

      checkExecutionModeDependencies(executionMode, arguments, console);

      switch (executionMode) {
        case ACCELERATED:
          return accelerated(newConfig(uri, console, arguments));
        case PARALLEL:
          return parallel(newConfig(uri, console, arguments));
        case YOUTUBE:
          final boolean noAudio = (Boolean) arguments.get(Argument.NO_AUDIO.value()).getValue();
          final boolean noVideo = (Boolean) arguments.get(Argument.NO_VIDEO.value()).getValue();
          return youtube(newConfig(uri, console, arguments), noAudio, noVideo);
        case UPLOAD:
          final URI requestBodyUri = (URI) arguments.get(Argument.REQUEST_BODY.value()).getValue();
          return upload(newConfig(uri, console, arguments), requestBodyUri);
        case SEQUENTIAL:
        default:
          return sequential(newConfig(uri, console, arguments));
      }
    } catch (IllegalArgumentException exc) {
      console.print(exc.getMessage());
      return HelpExecution.builder().console(console).build();
    } catch (Throwable throwable) {
      console.shutdown();
      throw throwable;
    }
  }

  /**
   * Accelerated execution.
   *
   * @param config the config
   * @return the execution
   */
  public static Execution accelerated(final ExecutionConfig config) {
    return AcceleratedGetExecution.builder()
        .config(config.getConfig())
        .authentication(config.getAuthentication())
        .prefetchAccessTokens(true)
        .uri(config.getUri())
        .responseOutputUri(config.getResponseOutputUri())
        .executorCount(config.getParallelThreads())
        .console(config.getConsole())
        .headers(config.getHeaders())
        .build();
  }

  /**
   * Parallel execution.
   *
   * @param config the config
   * @return the execution
   */
  public static Execution parallel(final ExecutionConfig config) {
    return ParallelExecution.builder()
        .config(config.getConfig())
        .authentication(config.getAuthentication())
        .prefetchAccessTokens(true)
        .uri(config.getUri())
        .responseOutputUri(config.getResponseOutputUri())
        .executorCount(config.getParallelThreads())
        .console(config.getConsole())
        .headers(config.getHeaders())
        .httpMethod(config.getHttpMethod())
        .repeat(config.getRepeat())
        .build();
  }

  /**
   * Youtube execution.
   *
   * @param config the config
   * @param noAudio the no audio
   * @param noVideo the no video
   * @return the execution
   */
  public static Execution youtube(
      final ExecutionConfig config, final boolean noAudio, final boolean noVideo) {
    return YouTubeExecution.youtubeExecutionBuilder()
        .config(config.getConfig())
        .uri(config.getUri())
        .responseOutputUri(config.getResponseOutputUri())
        .executorCount(config.getParallelThreads())
        .console(config.getConsole())
        .headers(config.getHeaders())
        .noAudio(noAudio)
        .noVideo(noVideo)
        .build();
  }

  /**
   * Sequential execution.
   *
   * @param config the config
   * @return the execution
   */
  public static Execution sequential(final ExecutionConfig config) {
    return SequentialExecution.builder()
        .config(config.getConfig())
        .authentication(config.getAuthentication())
        .prefetchAccessTokens(true)
        .httpMethod(config.getHttpMethod())
        .repeat(config.getRepeat())
        .uri(config.getUri())
        .responseOutputUri(config.getResponseOutputUri())
        .console(config.getConsole())
        .headers(config.getHeaders())
        .build();
  }

  /**
   * Upload execution.
   *
   * @param config the config
   * @param requestBodyUri the request body uri
   * @return the execution
   */
  public static Execution upload(final ExecutionConfig config, final URI requestBodyUri) {
    return UploadExecution.builder()
        .config(config.getConfig())
        .authentication(config.getAuthentication())
        .prefetchAccessTokens(true)
        .repeat(config.getRepeat())
        .uri(config.getUri())
        .responseOutputUri(config.getResponseOutputUri())
        .console(config.getConsole())
        .headers(config.getHeaders())
        .requestBodyUri(requestBodyUri)
        .httpMethod(config.getHttpMethod())
        .build();
  }

  /**
   * New authentication authentication.
   *
   * @param arguments the arguments
   * @return the authentication
   */
  protected static Authentication newAuthentication(final Map<String, ArgumentValue<?>> arguments) {
    final ArgumentValue<?> argumentValue = arguments.get(Argument.AUTHENTICATION.value());
    if (Objects.isNull(argumentValue)) {
      return null;
    }
    final AuthenticationMode authenticationMode = (AuthenticationMode) argumentValue.getValue();
    switch (authenticationMode) {
      case BASIC:
      case OAUTH:
      case BEARER_TOKEN:
        throw new UnsupportedOperationException(
            String.format("Authentication %s is not supported yet", authenticationMode.value()));
      case OAUTH2:
      default:
        return OAuth2.builder()
            .uri((URI) arguments.get(Argument.OAUTH2_HOST.value()).getValue())
            .clientId(arguments.get(Argument.OAUTH2_CLIENT_ID.value()).getValue().toString())
            .secret(arguments.get(Argument.OAUTH2_SECRET.value()).getValue().toString())
            .grantType(
                (OAuth2.GrantType) arguments.get(Argument.OAUTH2_GRANT_TYPE.value()).getValue())
            .build();
    }
  }

  /**
   * New config execution config.
   *
   * @param uri the uri
   * @param console the console
   * @param arguments the arguments
   * @return the execution config
   */
  protected static ExecutionConfig newConfig(
      final URI uri, final Console console, final Map<String, ArgumentValue<?>> arguments) {
    final SnoopyConfig config = newSnoopyConfig(arguments);
    final Authentication authentication = newAuthentication(arguments);
    final HttpMethod httpMethod =
        (HttpMethod) arguments.get(Argument.HTTP_METHOD.value()).getValue();
    final int repeat = (Integer) arguments.get(Argument.REPEAT.value()).getValue();
    final int parallelThreads =
        (Integer) arguments.get(Argument.PARALLEL_THREADS.value()).getValue();
    final ArgumentValue<?> argumentValue = arguments.get(Argument.RESPONSE_BODY_DUMP.value());
    final URI responseOutputUri =
        Objects.nonNull(argumentValue) && argumentValue.getValue() instanceof URI
            ? (URI) argumentValue.getValue()
            : null;
    final Map<CharSequence, List<CharSequence>> headers = new HashMap<>();
    headers.put(
        HttpHeaderNames.USER_AGENT,
        Collections.singletonList(
            arguments.get(Argument.USER_AGENT.value()).getValue().toString()));
    return ExecutionConfig.builder()
        .config(config)
        .authentication(authentication)
        .repeat(repeat)
        .httpMethod(httpMethod)
        .parallelThreads(parallelThreads)
        .console(console)
        .headers(headers)
        .responseOutputUri(responseOutputUri)
        .uri(uri)
        .build();
  }

  /**
   * New snoopy config snoopy config.
   *
   * @param arguments the arguments
   * @return the snoopy config
   */
  protected static SnoopyConfig newSnoopyConfig(final Map<String, ArgumentValue<?>> arguments) {
    return SnoopyConfig.builder()
        .connectionTimeout(
            TimeUnit.SECONDS.toMillis(
                (Integer) arguments.get(Argument.CONNECTION_TIMEOUT.value()).getValue()))
        .maximumHostConnections(
            (Integer) arguments.get(Argument.CONNECTIONS_PER_HOST.value()).getValue())
        .receiveBufferSize((Long) arguments.get(Argument.RECEIVE_BUFFER_SIZE.value()).getValue())
        .sendBufferSize((Long) arguments.get(Argument.SEND_BUFFER_SIZE.value()).getValue())
        .writeBufferLowWaterMark(
            (Long) arguments.get(Argument.WRITE_BUFFER_LOW_WATERMARK.value()).getValue())
        .writeBufferHighWaterMark(
            (Long) arguments.get(Argument.WRITE_BUFFER_HIGH_WATERMARK.value()).getValue())
        .tcpNoDelay((Boolean) arguments.get(Argument.TCP_NO_DELAY.value()).getValue())
        .workerExecutorThreads((Integer) arguments.get(Argument.WORKER_THREADS.value()).getValue())
        .insecure((Boolean) arguments.get(Argument.INSECURE.value()).getValue())
        .cachedConnectionPool()
        .build();
  }

  private static boolean isYouTube(final URI uri) {
    final Matcher matcher =
        Pattern.compile("https://www.youtube.com/watch\\?v=(.){11}$").matcher(uri.toString());
    return matcher.find();
  }

  private static boolean isFileUpload(final Map<String, ArgumentValue<?>> arguments) {
    final ArgumentValue<?> requestBodyArg = arguments.get(Argument.REQUEST_BODY.value());
    final ArgumentValue<?> httpMethodArg = arguments.get(Argument.HTTP_METHOD.value());
    return Objects.nonNull(requestBodyArg)
        && Objects.nonNull(requestBodyArg.getValue())
        && (httpMethodArg.isDefaultValue());
  }

  private static boolean isFileDownload(
      final Console console, final URI uri, final Map<String, ArgumentValue<?>> arguments) {
    final Snoopy snoopy = Snoopy.builder().config(SnoopyConfig.defaults()).build();
    final CharSequence contentType =
        snoopy
            .head(uri)
            .followRedirects(true)
            .failIfNotSuccessfulResponse(true)
            .connectionRetry(3)
            .execute()
            .map(SnoopyHttpResponse::contentType)
            .doOnError(throwable -> console.print("Warning: Unknown content type"))
            .doOnSuccess(type -> console.print(String.format("Content type: %s detected", type)))
            .doOnSubscribe(disposable -> console.print("HEAD content type check..."))
            .onErrorResumeWith(
                snoopy
                    .get(uri)
                    .ignoreResponseBody(true)
                    .followRedirects(true)
                    .failIfNotSuccessfulResponse(true)
                    .connectionRetry(3)
                    .execute()
                    .map(SnoopyHttpResponse::contentType)
                    .doOnError(throwable -> console.print("Warning: Unknown content type"))
                    .doOnSuccess(
                        type -> console.print(String.format("Content type: %s detected", type)))
                    .doOnSubscribe(
                        disposable -> console.print("Falling back to GET content type check..."))
                    .onErrorResumeWith(Single.just("")))
            .blockingGet();
    return StringUtils.isNoneBlank(contentType) && !HttpUtils.isText(contentType);
  }

  private static void overrideAcceleratedGetDefaults(
      final Map<String, ArgumentValue<?>> arguments) {
    ArgumentValue<?> argumentValue = arguments.get(Argument.EXECUTION_MODE.value());
    if (argumentValue.isDefaultValue()) {
      arguments.put(
          Argument.EXECUTION_MODE.value(),
          ArgumentValue.builder()
              .argument(Argument.EXECUTION_MODE)
              .value(ExecutionMode.ACCELERATED)
              .defaultValue(true)
              .build());
    }
  }

  private static void overrideYouTubeExecutionDefaults(
      final Map<String, ArgumentValue<?>> arguments) {
    ArgumentValue<?> argumentValue = arguments.get(Argument.EXECUTION_MODE.value());
    if (argumentValue.isDefaultValue()) {
      arguments.put(
          Argument.EXECUTION_MODE.value(),
          ArgumentValue.builder()
              .argument(Argument.EXECUTION_MODE)
              .value(ExecutionMode.YOUTUBE)
              .build());
    }
  }

  private static void overrideUploadExecutionDefaults(
      final Map<String, ArgumentValue<?>> arguments) {
    ArgumentValue<?> argumentValue = arguments.get(Argument.EXECUTION_MODE.value());
    if (argumentValue.isDefaultValue()) {
      arguments.put(
          Argument.EXECUTION_MODE.value(),
          ArgumentValue.builder()
              .argument(Argument.EXECUTION_MODE)
              .value(ExecutionMode.UPLOAD)
              .build());
    }
  }

  private static void checkExecutionModeDependencies(
      final ExecutionMode executionMode,
      Map<String, ArgumentValue<?>> arguments,
      final Console console) {
    Optional.ofNullable(executionMode.dependencies())
        .orElseGet(() -> Collections.emptyList())
        .forEach(
            dependency -> {
              final ArgumentValue<?> argVal = arguments.get(dependency.value());
              if (Objects.isNull(argVal)) {
                console.print(
                    String.format(
                        "%s dependency argument %s is missing",
                        executionMode.value(), dependency.value()));
                throw new IllegalArgumentException(
                    String.format(
                        "%s dependency argument %s is missing",
                        executionMode.value(), dependency.value()));
              }
            });
  }

  private static void checkArgumentDependencies(final Map<String, ArgumentValue<?>> arguments) {
    arguments
        .values()
        .forEach(
            argValue -> {
              checkDependencies(
                  argValue.getArgument().value(), argValue.getArgument().dependencies(), arguments);
              if (argValue.getValue() instanceof AuthenticationMode) {
                final AuthenticationMode authenticationMode =
                    (AuthenticationMode) argValue.getValue();
                checkDependencies(
                    String.format(
                        "%s %s", argValue.getArgument().value(), authenticationMode.value()),
                    authenticationMode.dependencies(),
                    arguments);
              }
            });
  }

  private static void checkDependencies(
      String name, List<Argument> dependencies, Map<String, ArgumentValue<?>> arguments) {
    Optional.ofNullable(dependencies)
        .ifPresent(
            deps ->
                deps.forEach(
                    dependency -> {
                      final ArgumentValue<?> argVal = arguments.get(dependency.value());
                      if (Objects.isNull(argVal)) {
                        throw new IllegalArgumentException(
                            String.format(
                                "%s dependency argument %s is missing", name, dependency.value()));
                      }
                    }));
  }

  private static HashMap<String, ArgumentValue<?>> parseArguments(String... args) {

    final int limit = args.length - 1;
    final HashMap<String, ArgumentValue<?>> arguments = createArgumentsWithDefaults();

    for (int i = 0; i < limit; i++) {

      final String arg = args[i];

      if (!StringUtils.startsWithAny(arg, "--", "-")) {
        throw new IllegalArgumentException(String.format("Invalid argument %s", arg));
      }

      final Argument argument = Validators.validateArgument(arg);

      if (Objects.isNull(argument)) {
        throw new IllegalArgumentException(String.format("Invalid argument %s", arg));
      }

      if (!argument.needsValue()) {
        arguments.put(
            arg,
            ArgumentValue.builder().argument(argument).value(true).defaultValue(false).build());
        continue;
      }

      final String value = args[++i];

      if (StringUtils.startsWithAny(value, "--", "-")
          || StringUtils.isBlank(value.replaceAll("\"", ""))) {
        throw new IllegalArgumentException(
            String.format("Invalid value %s for argument %s", value, arg));
      }

      switch (argument) {
        case AUTHENTICATION:
          final AuthenticationMode authenticationMode =
              Validators.validateAuthenticationMode(value);
          if (Objects.nonNull(authenticationMode)) {
            arguments.put(
                arg, ArgumentValue.builder().argument(argument).value(authenticationMode).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case REQUEST_BODY_DUMP:
        case RESPONSE_BODY_DUMP:
          final OutputMode bodyOutput = Validators.validateOutputMode(value);
          if (bodyOutput == OutputMode.CONSOLE) {
            arguments.put(
                arg, ArgumentValue.builder().argument(argument).value(bodyOutput).build());
            break;
          } else {
            final URI bodyOutputUri = Validators.validateUri(value);
            if (Objects.nonNull(bodyOutputUri)) {
              arguments.put(
                  arg, ArgumentValue.builder().argument(argument).value(bodyOutputUri).build());
              break;
            }
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case REQUEST_BODY:
          final Path requestInputPath = Validators.validatePath(value);
          if (Objects.nonNull(requestInputPath)) {
            arguments.put(
                arg,
                ArgumentValue.builder().argument(argument).value(requestInputPath.toUri()).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case OAUTH2_HOST:
          final URI oauthHostUri = Validators.validateUri(value);
          if (Objects.nonNull(oauthHostUri)) {
            arguments.put(
                arg, ArgumentValue.builder().argument(argument).value(oauthHostUri).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case OAUTH2_GRANT_TYPE:
          final OAuth2.GrantType grantType = Validators.validateGrantType(value);
          if (Objects.nonNull(grantType)) {
            arguments.put(arg, ArgumentValue.builder().argument(argument).value(grantType).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case OAUTH2_SECRET:
        case OAUTH2_CLIENT_ID:
        case USER_AGENT:
          if (StringUtils.isNoneBlank(value)) {
            arguments.put(arg, ArgumentValue.builder().argument(argument).value(value).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case WORKER_THREADS:
        case PARALLEL_THREADS:
        case CONNECTIONS_PER_HOST:
        case REPEAT:
          final Integer integer = Validators.validateInteger(value);
          if (Objects.nonNull(integer)) {
            arguments.put(arg, ArgumentValue.builder().argument(argument).value(integer).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case HTTP_METHOD:
          final HttpMethod httpMethod = Validators.validateHttpMethod(value);
          if (Objects.nonNull(httpMethod)) {
            arguments.put(
                arg, ArgumentValue.builder().argument(argument).value(httpMethod).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case SEND_BUFFER_SIZE:
        case RECEIVE_BUFFER_SIZE:
        case WRITE_BUFFER_LOW_WATERMARK:
        case WRITE_BUFFER_HIGH_WATERMARK:
          final Long bufferSize = Validators.validateILong(value);
          if (Objects.nonNull(bufferSize)) {
            arguments.put(
                arg, ArgumentValue.builder().argument(argument).value(bufferSize).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));

        case EXECUTION_MODE:
          final ExecutionMode executionMode = Validators.validateExecutionMode(value);
          if (Objects.nonNull(executionMode)) {
            arguments.put(
                arg, ArgumentValue.builder().argument(argument).value(executionMode).build());
            break;
          }
          throw new IllegalArgumentException(
              String.format("Invalid value %s for argument %s", value, arg));
      }
    }
    return arguments;
  }

  private static HashMap<String, ArgumentValue<?>> createArgumentsWithDefaults() {
    return Arrays.stream(Argument.values())
        .filter(Argument::hasDefaultValue)
        .collect(
            HashMap::new,
            (args, arg) ->
                args.put(
                    arg.value(),
                    ArgumentValue.builder()
                        .argument(arg)
                        .value(arg.defaultValue())
                        .defaultValue(true)
                        .build()),
            (args1, args2) -> {});
  }

  private Executions() {
    throw new AssertionError("Executions cannot be instantiated");
  }
}
