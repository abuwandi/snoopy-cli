package org.bitbucket.abuwandi.snoopy.standalone.execution;

import lombok.Builder;
import lombok.Getter;

/**
 * The type Argument value.
 *
 * @param <T> the type parameter
 */
@Getter
public class ArgumentValue<T> {

  private final Argument argument;
  private final T value;
  private final boolean defaultValue;

  /**
   * Instantiates a new Argument value.
   *
   * @param argument the argument
   * @param value the value
   * @param defaultValue the default value
   */
  @Builder
  public ArgumentValue(final Argument argument, final T value, final boolean defaultValue) {
    this.argument = argument;
    this.value = value;
    this.defaultValue = defaultValue;
  }
}
