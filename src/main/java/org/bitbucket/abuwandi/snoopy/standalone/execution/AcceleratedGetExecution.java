package org.bitbucket.abuwandi.snoopy.standalone.execution;

import io.netty.handler.codec.http.HttpHeaderValues;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.tika.mime.MimeTypes;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.Status;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpResponse;
import org.bitbucket.abuwandi.snoopy.client.http.util.Extractors;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ConsoleProgress;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;
import org.bitbucket.abuwandi.snoopy.standalone.output.ResourcesUtilizationReport;
import org.bitbucket.abuwandi.snoopy.standalone.output.TimeReport;
import org.bitbucket.abuwandi.snoopy.standalone.util.Formatters;
import org.bitbucket.abuwandi.snoopy.standalone.util.HttpUtils;
import org.bitbucket.abuwandi.snoopy.standalone.util.ProgressStatus;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;
import org.bitbucket.abuwandi.snoopy.util.Progress;
import org.bitbucket.abuwandi.snoopy.util.RetryStrategy;
import org.bitbucket.abuwandi.snoopy.util.StopWatch;

/** The type Accelerated get execution. */
public class AcceleratedGetExecution extends HttpExecution {

  @Override
  protected void preExecute() {
    final int receiveBufferSize =
        (int) Math.max(config.getReceiveBufferSize(), ByteUnit.MIB.toBytes(8));
    final int connectionsPerHost = Math.max(config.getMaximumHostConnections(), executorCount);
    config.setMaximumHostConnections(connectionsPerHost);
    config.setReceiveBufferSize(receiveBufferSize);
    config.setReadTimeout(15);
    config.setReadTimeoutUnit(TimeUnit.SECONDS);
    config.setIdleTimeout(10);
    config.setReadTimeoutUnit(TimeUnit.SECONDS);
    // config.setWriteBufferLowWaterMark();
    // config.setWriteBufferHighWaterMark();
    super.preExecute();
  }

  @Override
  public ExecutionReport doExecute(final URI uri, final URI outputUri) {
    final AtomicInteger counter = new AtomicInteger(0);
    try {
      final AcceleratedRequest acceleratedRequest = partitionRequest(uri, outputUri);
      final Path finalPath =
          Paths.get(Optional.ofNullable(outputUri).orElse(acceleratedRequest.getOutputUri()));

      final StopWatch stopWatch = StopWatch.builder().build();
      final Disposable resourcesMonitorDisposable = watchResources();

      final AcceleratedResponse acceleratedResponse =
          Flowable.fromIterable(acceleratedRequest.getPartialRequests())
              .flatMapMaybe(
                  partialRequest -> {
                    final int count = counter.incrementAndGet();
                    return Maybe.just(partialRequest)
                        .flatMap(
                            request ->
                                request
                                    .requestBuilder
                                    .reference(String.valueOf(count))
                                    .headers(headers)
                                    .execute()
                                    .flatMapMaybe(
                                        response ->
                                            handle(
                                                response,
                                                generateFilePartName(
                                                    finalPath.getFileName().toString(), count)))
                                    .subscribeOn(downloadScheduler)
                                    .doOnSuccess(disposal -> partialRequest.progress.updateValue(0))
                                    .doFinally(stopWatch::time))
                        .retryWhen(partialRequest.retryStrategy.retry())
                        .onErrorResumeNext(this::handleParallelError);
                  })
              .doOnSubscribe(subscription -> stopWatch.start())
              .observeOn(singleThreadScheduler)
              .subscribeOn(singleThreadScheduler)
              .collect(LinkedList<PartialResponse>::new, LinkedList::add)
              .map(
                  partialResponses -> {
                    resourcesMonitorDisposable.dispose();
                    return sortAndMerge(finalPath, partialResponses);
                  })
              .blockingGet();
      console.print(
          String.format(
              "Download total time: %s", Formatters.formatMillisShort(stopWatch.total())));
      final ExecutionReport executionReport =
          ExecutionReport.builder()
              .name(ExecutionMode.ACCELERATED.value())
              .status(ExecutionStatus.SUCCESS)
              .timeReport(
                  TimeReport.builder()
                      .connectionTime(acceleratedResponse.getConnectionTime())
                      .requestTime(acceleratedResponse.getRequestTime())
                      .responseTime(acceleratedResponse.getResponseTime())
                      .build())
              .line(console.newLine())
              .build();
      console.print(executionReport);
      return executionReport;
    } catch (ExecutionException exc) {
      throw exc;
    } catch (Exception exc) {
      throw new ExecutionException("Execution failed", exc);
    }
  }

  @Override
  public void shutdown(final Throwable throwable) {
    super.shutdown(throwable);
  }

  protected Disposable watchResources() {
    console.print();
    final ResourcesUtilizationReport resourcesUtilizationReport =
        ResourcesUtilizationReport.builder()
            .title("System Resources Monitor:")
            .console(console)
            .line(console.newLine())
            .build();
    installConnectionPoolStatsListener(resourcesUtilizationReport);
    resourcesUtilizationReport.update();
    console.print();
    return Observable.intervalRange(0, Integer.MAX_VALUE, 0, 1, TimeUnit.SECONDS)
        .doFinally(() -> uninstallConnectionPoolStatsListener(resourcesUtilizationReport))
        .subscribe(counter -> resourcesUtilizationReport.update());
  }

  private URI generateFilePartName(final String filename, final int partNumber) {
    return Paths.get(
            System.getProperty("user.dir"), String.format("%s.part-%d", filename, partNumber))
        .toUri();
  }

  private Maybe<PartialResponse> handleParallelError(final Throwable throwable) {
    if (firstError.compareAndSet(null, throwable)) {
      return Maybe.error(
          Optional.of(throwable)
              .filter(Error.class::isInstance)
              .orElseGet(() -> new ExecutionException(throwable)));
    }
    return Maybe.empty();
  }

  private List<PartialResponse> sort(final List<PartialResponse> paths) {
    if (paths.isEmpty() || paths.size() < 2) {
      return paths;
    }
    paths.sort(
        (partialResponse1, partialResponse2) -> {
          final String filename1 = partialResponse1.getPath().getFileName().toString();
          final int order1 = Integer.parseInt(filename1.substring(filename1.lastIndexOf('.') + 5));
          final String filename2 = partialResponse2.getPath().getFileName().toString();
          final int order2 = Integer.parseInt(filename2.substring(filename2.lastIndexOf('.') + 5));
          return order2 - order1;
        });
    return paths;
  }

  private AcceleratedResponse sortAndMerge(
      final Path finalPath, final List<PartialResponse> partialResponses) {
    final List<PartialResponse> sortedPartialResponses = sort(partialResponses);
    OutputStream outputStream = null;
    WritableByteChannel dst = null;
    ConsoleProgress progress = null;
    try {
      console.print("Merging file parts...");
      console.print();
      final AtomicLong connectionTime = new AtomicLong(0);
      final AtomicLong requestTime = new AtomicLong(0);
      final AtomicLong responseTime = new AtomicLong(0);
      final AtomicLong totalSize = new AtomicLong(0);
      sortedPartialResponses.forEach(
          partialResponse -> {
            connectionTime.addAndGet(partialResponse.getConnectionTime());
            requestTime.addAndGet(partialResponse.getRequestTime());
            responseTime.addAndGet(partialResponse.getResponseTime());
            totalSize.addAndGet(
                Optional.ofNullable(partialResponse.getPath())
                    .map(path -> path.toFile().length())
                    .orElse(0L));
          });
      progress =
          newProgressBar(
              StringUtils.abbreviateMiddle(finalPath.getFileName().toString(), "...", 23),
              totalSize.get());
      outputStream = Files.newOutputStream(finalPath);
      dst = Channels.newChannel(outputStream);
      final WritableByteChannel finalDst = dst;
      final ByteBuffer buffer = ByteBuffer.allocateDirect(IOUtils.DEFAULT_BUFFER_SIZE);
      progress.updateStatus(ProgressStatus.ACTIVE);
      console.print();
      final ConsoleProgress finalProgress = progress;
      sortedPartialResponses.forEach(
          partialResponse -> {
            InputStream inputStream = null;
            ReadableByteChannel src = null;
            try {
              inputStream = Files.newInputStream(partialResponse.getPath());
              src = Channels.newChannel(inputStream);
              buffer.clear();
              org.bitbucket.abuwandi.snoopy.standalone.util.IOUtils.copy(
                  src, finalDst, buffer, finalProgress);
              IOUtils.close(src, inputStream);
              IOUtils.safeDelete(partialResponse.getPath());
            } catch (Exception exc) {
              IOUtils.close(src, inputStream);
              throw new ExecutionException("File parts merge failed", exc);
            } catch (Throwable throwable) {
              IOUtils.close(src, inputStream);
              throw throwable;
            }
          });
      progress.updateStatus(ProgressStatus.COMPLETE);
      console.print(
          String.format(
              "Merged file parts into %s",
              StringUtils.abbreviateMiddle(finalPath.toString(), "...", 50)));
      return AcceleratedResponse.builder()
          .path(finalPath)
          .connectionTime(connectionTime.get())
          .requestTime(requestTime.get())
          .responseTime(responseTime.get())
          .build();
    } catch (ExecutionException exc) {
      if (Objects.nonNull(progress)) {
        progress.updateStatus(ProgressStatus.FAILED);
      }
      throw exc;
    } catch (IOException exc) {
      if (Objects.nonNull(progress)) {
        progress.updateStatus(ProgressStatus.FAILED);
      }
      throw new ExecutionException("File parts merge failed", exc);
    } finally {
      IOUtils.close(dst, outputStream);
    }
  }

  private AcceleratedRequest partitionRequest(final URI uri, final URI outputUri) {
    final PartialRequestSupportResponse response =
        createRequestBuilder(HttpMethod.HEAD, uri)
            .headers(headers)
            .execute()
            .doOnSubscribe(disposable -> console.print("HEAD partial requests support check..."))
            .flatMap(this::handleInfoResponse)
            .onErrorResumeWith(
                createGetRequestBuilder(uri)
                    .ignoreResponseBody(true)
                    .headers(headers)
                    .execute()
                    .doOnSubscribe(
                        disposable ->
                            console.print("Falling back to GET partial requests support check..."))
                    .flatMap(this::handleInfoResponse))
            .blockingGet();

    if (response.getContentLength() < 1) {
      throw new ExecutionException(
          String.format("Invalid content length: %s", response.getContentLength()));
    }

    if (response.isPartialRequestsSupported()) {
      console.print("Server supports partial requests");
      console.print(String.format("Sending %d partial requests for file download", executorCount));
      console.print();
      return AcceleratedRequest.builder()
          .partialRequests(breakRequest(uri, executorCount, response.getContentLength()))
          .outputUri(response.getPath().toUri())
          .build();
    }
    console.print("Server doesn't support partial requests");
    console.print("Sending one request for file download");
    console.print();
    final Path path =
        Paths.get(Optional.ofNullable(outputUri).orElseGet(() -> response.getPath().toUri()));
    final ConsoleProgress progress =
        newProgressBar(
            StringUtils.abbreviateMiddle(String.format("%s", path.getFileName()), "...", 22),
            response.getContentLength());
    progress.updateValue(0);
    return AcceleratedRequest.builder()
        .partialRequests(
            Collections.singletonList(
                PartialRequest.builder()
                    .requestBuilder(
                        createGetRequestBuilder(uri)
                            .responseStatusCallback(
                                newStatus ->
                                    progress.updateStatus(
                                        Optional.of(newStatus)
                                            .map(Status::name)
                                            .map(ProgressStatus::valueOf)
                                            .get()))
                            .responseThroughputCallback(progress::updateThroughput)
                            .responseProgressCallback(progress::updateValue))
                    .progress(progress)
                    .retryStrategy(
                        RetryStrategy.builder()
                            .maxAttempts(3)
                            .lowestDelay(200)
                            .growBy(100)
                            .highestDelay(500)
                            .unit(TimeUnit.MILLISECONDS)
                            .errorPredicate(throwable -> true)
                            .onRetryConsumer(
                                (attempt, error) -> {
                                  if (attempt < 6) {
                                    progress.reset();
                                  }
                                })
                            .build())
                    .build()))
        .outputUri(path.toUri())
        .build();
  }

  private Single<PartialRequestSupportResponse> handleInfoResponse(
      final SnoopyHttpResponse response) {
    final String filename =
        Optional.ofNullable(response.filename())
            .map(CharSequence::toString)
            .orElseGet(
                () -> {
                  console.print("Warning: File name not found");
                  return String.format("response_%s", System.nanoTime());
                });
    final String contentType =
        Optional.ofNullable(response.contentType())
            .map(CharSequence::toString)
            .orElseGet(
                () -> {
                  console.print("Warning: Unknown content type");
                  return HttpHeaderValues.APPLICATION_OCTET_STREAM.toString();
                });
    final String ext = getExtension(contentType, ".bin");
    final Path path = Paths.get(System.getProperty("user.dir"), filename + ext);
    return Single.just(
        PartialRequestSupportResponse.builder()
            .contentLength(response.contentLength())
            .partialRequestsSupported(response.supportPartialRequests())
            .path(path)
            .build());
  }

  private String getExtension(final String contentType, final String defaultExtension) {
    try {
      return Optional.ofNullable(
              MimeTypes.getDefaultMimeTypes().forName(contentType).getExtension())
          .orElse(defaultExtension);
    } catch (Exception exc) {
      return defaultExtension;
    }
  }

  private List<PartialRequest> breakRequest(
      final URI uri, final int requestsNumber, final long totalContentLength) {
    final List<PartialRequest> requests = new LinkedList<>();
    final long partSize = totalContentLength / requestsNumber;
    final long remainingSize = totalContentLength % requestsNumber;
    long from = 0;
    long to = partSize - 1;
    int counter = 0;

    while (++counter < requestsNumber) {
      final ConsoleProgress progress =
          newProgressBar(String.format("Part %d", counter), (to - from) + 1);
      progress.updateValue(0);
      requests.add(
          PartialRequest.builder()
              .requestBuilder(
                  createGetRequestBuilder(uri)
                      .range(from, to)
                      .responseStatusCallback(
                          newStatus ->
                              progress.updateStatus(
                                  Optional.of(newStatus)
                                      .map(Status::name)
                                      .map(ProgressStatus::valueOf)
                                      .get()))
                      .responseThroughputCallback(progress::updateThroughput)
                      .responseProgressCallback(progress::updateValue))
              .progress(progress)
              .retryStrategy(
                  RetryStrategy.builder()
                      .maxAttempts(3)
                      .lowestDelay(200)
                      .growBy(100)
                      .highestDelay(500)
                      .unit(TimeUnit.MILLISECONDS)
                      .errorPredicate(throwable -> true)
                      .onRetryConsumer(
                          (attempt, error) -> {
                            if (attempt < 6) {
                              progress.reset();
                            }
                          })
                      .build())
              .build());
      from += partSize;
      to += partSize;
    }
    final ConsoleProgress progress =
        newProgressBar(String.format("Part %d", counter), (to - from) + remainingSize + 1);
    progress.updateValue(0);
    requests.add(
        PartialRequest.builder()
            .requestBuilder(
                createGetRequestBuilder(uri)
                    .range(from, to + remainingSize)
                    .responseStatusCallback(
                        newStatus ->
                            progress.updateStatus(
                                Optional.of(newStatus)
                                    .map(Status::name)
                                    .map(ProgressStatus::valueOf)
                                    .get()))
                    .responseThroughputCallback(progress::updateThroughput)
                    .responseProgressCallback(progress::updateValue))
            .progress(progress)
            .retryStrategy(
                RetryStrategy.builder()
                    .maxAttempts(3)
                    .lowestDelay(200)
                    .growBy(100)
                    .highestDelay(500)
                    .unit(TimeUnit.MILLISECONDS)
                    .errorPredicate(throwable -> true)
                    .onRetryConsumer(
                        (attempt, error) -> {
                          if (attempt < 6) {
                            progress.reset();
                          }
                        })
                    .build())
            .build());
    return requests;
  }

  protected Maybe<PartialResponse> handle(
      @NonNull final SnoopyHttpResponse response, final URI responseBodyOutput) {
    try {
      if (!HttpUtils.isText(response.contentType())) {
        final Path responsePath = Paths.get(responseBodyOutput);
        return Extractors.saveBodyToFile(response, responsePath)
            .map(
                path ->
                    PartialResponse.builder()
                        .path(path)
                        .requestTime(response.request().time())
                        .connectionTime(response.connectionTime())
                        .responseTime(response.time())
                        .build())
            .toMaybe();
      }
      return Extractors.disposeBody(response)
          .map(
              disposed ->
                  PartialResponse.builder()
                      .requestTime(response.request().time())
                      .connectionTime(response.connectionTime())
                      .responseTime(response.time())
                      .build());
    } catch (Throwable throwable) {
      return Maybe.error(throwable);
    }
  }

  @Getter
  private static class AcceleratedRequest {

    private final List<PartialRequest> partialRequests;
    private final URI outputUri;

    /**
     * Instantiates a new Accelerated request.
     *
     * @param partialRequests the partial requests
     * @param outputUri the output uri
     */
    @Builder
    AcceleratedRequest(final List<PartialRequest> partialRequests, final URI outputUri) {
      this.partialRequests = partialRequests;
      this.outputUri = outputUri;
    }
  }

  @Getter
  private static class AcceleratedResponse {

    private final Path path;
    private final long connectionTime;
    private final long requestTime;
    private final long responseTime;

    /**
     * Instantiates a new Accelerated response.
     *
     * @param path the path
     * @param connectionTime the connection time
     * @param requestTime the request time
     * @param responseTime the response time
     */
    @Builder
    AcceleratedResponse(
        final Path path,
        final long connectionTime,
        final long requestTime,
        final long responseTime) {
      this.path = path;
      this.connectionTime = connectionTime;
      this.requestTime = requestTime;
      this.responseTime = responseTime;
    }
  }

  @Getter
  private static class PartialResponse {

    private final Path path;
    private final long requestTime;
    private final long connectionTime;
    private final long responseTime;

    /**
     * Instantiates a new Partial response.
     *
     * @param path the path
     * @param requestTime the request time
     * @param connectionTime the connection time
     * @param responseTime the response time
     */
    @Builder
    public PartialResponse(
        final Path path,
        final long requestTime,
        final long connectionTime,
        final long responseTime) {
      this.path = path;
      this.requestTime = requestTime;
      this.connectionTime = connectionTime;
      this.responseTime = responseTime;
    }
  }

  @Getter
  private static class PartialRequestSupportResponse {

    private final boolean partialRequestsSupported;
    private final long contentLength;
    private final Path path;

    /**
     * Instantiates a new Partial request support response.
     *
     * @param path the path
     * @param contentLength the content length
     * @param partialRequestsSupported the partial requests supported
     */
    @Builder
    PartialRequestSupportResponse(
        final Path path, final long contentLength, final boolean partialRequestsSupported) {
      this.path = path;
      this.contentLength = contentLength;
      this.partialRequestsSupported = partialRequestsSupported;
    }
  }

  private static class PartialRequest {

    private final HttpRequestBuilder requestBuilder;
    private final Progress progress;
    private final RetryStrategy retryStrategy;

    /**
     * Instantiates a new Partial request.
     *
     * @param requestBuilder the request builder
     * @param progress the progress
     * @param retryStrategy the retry strategy
     */
    @Builder
    public PartialRequest(
        final HttpRequestBuilder requestBuilder,
        final Progress progress,
        final RetryStrategy retryStrategy) {
      this.requestBuilder = requestBuilder;
      this.progress = progress;
      this.retryStrategy = retryStrategy;
    }
  }

  /** The Executor count. */
  protected final int executorCount;
  /** The First error. */
  protected final AtomicReference<Throwable> firstError;
  /** The Download scheduler. */
  protected final Scheduler downloadScheduler;
  /** The Single thread scheduler. */
  protected final Scheduler singleThreadScheduler;

  /**
   * Instantiates a new Accelerated get execution.
   *
   * @param config the config
   * @param authentication the authentication
   * @param prefetchAccessTokens the prefetch access tokens
   * @param console the console
   * @param uri the uri
   * @param responseOutputUri the response output uri
   * @param headers the headers
   * @param executorCount the executor count
   */
  @Builder
  public AcceleratedGetExecution(
      final SnoopyConfig config,
      final Authentication authentication,
      final boolean prefetchAccessTokens,
      final Console console,
      final URI uri,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers,
      final int executorCount) {
    super(
        config,
        prefetchAccessTokens,
        authentication,
        console,
        uri,
        HttpMethod.GET,
        responseOutputUri,
        headers);
    this.executorCount = executorCount;
    this.firstError = new AtomicReference<>(null);
    this.downloadScheduler = Schedulers.from(Executors.newFixedThreadPool(executorCount));
    this.singleThreadScheduler = Schedulers.from(Executors.newSingleThreadExecutor());
  }
}
