package org.bitbucket.abuwandi.snoopy.standalone.output;

/** The interface Printable. */
public interface Printable {

  /**
   * Line int.
   *
   * @return the int
   */
  int line();

  /**
   * Lines reserved int.
   *
   * @return the int
   */
  int linesReserved();

  /**
   * Print string.
   *
   * @return the string
   */
  String print();

  /**
   * Color console color.
   *
   * @return the console color
   */
  ConsoleColor color();

  /**
   * Is printed boolean.
   *
   * @return the boolean
   */
  boolean isPrinted();

  /**
   * Sets printed.
   *
   * @param printed the printed
   */
  void setPrinted(boolean printed);
}
