package org.bitbucket.abuwandi.snoopy.standalone.execution;

import java.net.URI;
import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;

/** The type Execution config. */
@Getter
public class ExecutionConfig {

  private final URI uri;
  private final SnoopyConfig config;
  private final Authentication authentication;
  private final Console console;
  private final int repeat;
  private final URI responseOutputUri;
  private final Map<CharSequence, List<CharSequence>> headers;
  private final int parallelThreads;
  private final HttpMethod httpMethod;

  /**
   * Instantiates a new Execution config.
   *
   * @param uri the uri
   * @param config the config
   * @param authentication the authentication
   * @param console the console
   * @param repeat the repeat
   * @param responseOutputUri the response output uri
   * @param headers the headers
   * @param parallelThreads the parallel threads
   * @param httpMethod the http method
   */
  @Builder
  public ExecutionConfig(
      final URI uri,
      final SnoopyConfig config,
      final Authentication authentication,
      final Console console,
      int repeat,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers,
      final int parallelThreads,
      final HttpMethod httpMethod) {
    this.uri = uri;
    this.config = config;
    this.authentication = authentication;
    this.console = console;
    this.repeat = repeat;
    this.responseOutputUri = responseOutputUri;
    this.headers = headers;
    this.parallelThreads = parallelThreads;
    this.httpMethod = httpMethod;
  }
}
