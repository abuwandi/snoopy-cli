package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.util.Arrays;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/** The enum Output mode. */
public enum OutputMode {
  /** Console output mode. */
  CONSOLE("console"),
  /** Uri output mode. */
  URI("uri");

  private final String value;

  OutputMode(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * For value output mode.
   *
   * @param name the name
   * @return the output mode
   */
  public static OutputMode forValue(final String name) {
    return Optional.ofNullable(name)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(OutputMode.values())
                    .filter(arg -> arg.value().equals(target))
                    .findFirst()
                    .orElse(URI))
        .orElseThrow(() -> new IllegalArgumentException("Invalid argument value"));
  }
}
