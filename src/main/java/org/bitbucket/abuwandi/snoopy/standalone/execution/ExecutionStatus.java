package org.bitbucket.abuwandi.snoopy.standalone.execution;

/** The enum Execution status. */
public enum ExecutionStatus {
  /** Success execution status. */
  SUCCESS("Success"),
  /** Failed execution status. */
  FAILED("Failed"),
  /** The In progress. */
  IN_PROGRESS("In progress");

  private final String value;

  ExecutionStatus(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return this.value;
  }
}
