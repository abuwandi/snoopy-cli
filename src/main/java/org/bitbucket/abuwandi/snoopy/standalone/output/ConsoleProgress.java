package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import lombok.Builder;
import lombok.NonNull;
import org.bitbucket.abuwandi.snoopy.standalone.util.Formatters;
import org.bitbucket.abuwandi.snoopy.standalone.util.ProgressStatus;
import org.bitbucket.abuwandi.snoopy.util.Progress;

/** The type Console progress. */
public class ConsoleProgress implements Printable, Progress {

  private final Console console;
  private final int line;
  private final long maxValue;
  private final AtomicLong value;
  private final AtomicLong throughput;
  private final AtomicReference<String> title;
  private final AtomicReference<ProgressStatus> status;
  private final int refreshEach;
  private final int linesReserved;
  private final AtomicBoolean printed;

  /**
   * Instantiates a new Console progress.
   *
   * @param title the title
   * @param status the status
   * @param console the console
   * @param line the line
   * @param maxValue the max value
   * @param throughput the throughput
   * @param value the value
   * @param refreshEach the refresh each
   * @param linesReserved the lines reserved
   */
  @Builder
  protected ConsoleProgress(
      @NonNull final String title,
      @NonNull final ProgressStatus status,
      @NonNull final Console console,
      final int line,
      final long maxValue,
      final long throughput,
      final long value,
      final int refreshEach,
      final int linesReserved) {
    this.title = new AtomicReference<>(title);
    this.status = new AtomicReference<>(status);
    this.throughput = new AtomicLong(throughput);
    this.console = console;
    this.line = line;
    this.maxValue = maxValue;
    this.value = new AtomicLong(value);
    this.refreshEach = Optional.of(refreshEach).filter(val -> val > 0 && val < 100).orElse(10);
    this.linesReserved = Math.max(linesReserved, 1);
    this.printed = new AtomicBoolean(false);
  }

  /** Reset. */
  public void reset() {
    this.value.set(0);
    this.status.set(ProgressStatus.PENDING);
    this.throughput.set(0);
    console.print(this);
  }

  @Override
  public int line() {
    return line;
  }

  @Override
  public String print() {
    return Formatters.format(this);
  }

  public float progress() {
    return ((float) value.get() / maxValue) * 100.0f;
  }

  /**
   * Completed boolean.
   *
   * @return the boolean
   */
  public boolean completed() {
    return value.get() == maxValue;
  }

  /**
   * Title string.
   *
   * @return the string
   */
  public String title() {
    return this.title.get();
  }

  /**
   * Status string.
   *
   * @return the string
   */
  public String status() {
    return this.status.get().value();
  }

  /**
   * Remaining long.
   *
   * @return the long
   */
  public long remaining() {
    return maxValue() - currentValue();
  }

  /**
   * Throughput long.
   *
   * @return the long
   */
  public long throughput() {
    return this.throughput.get();
  }

  public void updateValue(final long value) {
    this.value.updateAndGet(current -> value + current);
    final int currentProgress = (int) progress();
    if (currentProgress % refreshEach == 0) {
      console.print(this);
    }
  }

  /**
   * Update status.
   *
   * @param status the status
   */
  public void updateStatus(final ProgressStatus status) {
    final ProgressStatus before = this.status.get();
    final ProgressStatus after = this.status.updateAndGet(current -> status);
    if (before != after) {
      console.print(this);
    }
  }

  /**
   * Update throughput.
   *
   * @param throughput the throughput
   */
  public void updateThroughput(long throughput) {
    final long before = this.throughput.get();
    final long after = this.throughput.updateAndGet(current -> throughput);
    if (before != after) {
      console.print(this);
    }
  }

  /**
   * Update status and throughput.
   *
   * @param status the status
   * @param acceleration the acceleration
   */
  public void updateStatusAndThroughput(final ProgressStatus status, final long acceleration) {
    final long beforeAcceleration = this.throughput.get();
    final long afterAcceleration = this.throughput.updateAndGet(current -> acceleration);
    final ProgressStatus beforeStatus = this.status.get();
    final ProgressStatus afterStatus = this.status.updateAndGet(current -> status);
    if (beforeAcceleration != afterAcceleration || beforeStatus != afterStatus) {
      console.print(this);
    }
  }

  @Override
  public int linesReserved() {
    return linesReserved;
  }

  @Override
  public ConsoleColor color() {
    switch (status.get()) {
      case IDLE:
        return ConsoleColor.CYAN;
      case COMPLETE:
        return ConsoleColor.WHITE;
      case FAILED:
        return ConsoleColor.RED;
      default:
        return ConsoleColor.DEFAULT;
    }
  }

  /**
   * Current value long.
   *
   * @return the long
   */
  public long currentValue() {
    return value.get();
  }

  /**
   * Max value long.
   *
   * @return the long
   */
  public long maxValue() {
    return maxValue;
  }

  @Override
  public boolean isPrinted() {
    return this.printed.get();
  }

  @Override
  public void setPrinted(final boolean printed) {
    this.printed.compareAndSet(false, printed);
  }
}
