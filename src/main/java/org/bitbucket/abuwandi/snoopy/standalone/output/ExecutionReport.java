package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.util.concurrent.atomic.AtomicBoolean;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.abuwandi.snoopy.standalone.execution.ExecutionStatus;

/** The type Execution report. */
@Getter
public class ExecutionReport implements Printable {

  private final TimeReport timeReport;
  private final ExecutionStatus status;
  private final String name;

  @Getter(AccessLevel.NONE)
  private final int linesReserved;

  @Getter(AccessLevel.NONE)
  private final int line;

  private final AtomicBoolean printed;

  /**
   * Instantiates a new Execution report.
   *
   * @param name the name
   * @param timeReport the time report
   * @param status the status
   * @param line the line
   * @param linesReserved the lines reserved
   */
  @Builder
  public ExecutionReport(
      final String name,
      final TimeReport timeReport,
      final ExecutionStatus status,
      final int line,
      final int linesReserved) {
    this.name = name;
    this.timeReport = timeReport;
    this.status = status;
    this.line = line;
    this.linesReserved = Math.max(linesReserved, 1);
    this.printed = new AtomicBoolean(false);
  }

  @Override
  public int line() {
    return line;
  }

  @Override
  public String print() {
    return String.format("Execution: %s\tStatus: %s\t%s", name, status.value(), timeReport.print());
  }

  @Override
  public int linesReserved() {
    return linesReserved;
  }

  @Override
  public ConsoleColor color() {
    switch (status) {
      case FAILED:
        return ConsoleColor.RED;
      case SUCCESS:
        return ConsoleColor.GREEN;
      case IN_PROGRESS:
        return ConsoleColor.YELLOW;
      default:
        return ConsoleColor.DEFAULT;
    }
  }

  @Override
  public boolean isPrinted() {
    return this.printed.get();
  }

  @Override
  public void setPrinted(final boolean printed) {
    this.printed.compareAndSet(false, printed);
  }
}
