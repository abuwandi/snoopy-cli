package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionPoolStats;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionPoolStatsListener;
import org.bitbucket.abuwandi.snoopy.standalone.util.Formatters;

/** The type Resources utilization report. */
@Getter
public class ResourcesUtilizationReport implements Printable, ConnectionPoolStatsListener {

  private final AtomicLong totalMemory;
  private final AtomicLong usedMemory;
  private final AtomicLong timeCounter;
  private final Console console;
  private final String title;
  private final AtomicInteger connectionsTotal;
  private final AtomicInteger idleConnectionsTotal;
  private final AtomicInteger activeConnectionsTotal;

  @Getter(AccessLevel.NONE)
  private final int linesReserved;

  @Getter(AccessLevel.NONE)
  private final int line;

  @Getter(AccessLevel.NONE)
  private final AtomicBoolean printed;

  /**
   * Instantiates a new Resources utilization report.
   *
   * @param line the line
   * @param linesReserved the lines reserved
   * @param console the console
   * @param title the title
   */
  @Builder
  public ResourcesUtilizationReport(
      final int line, final int linesReserved, final Console console, final String title) {
    this.line = line;
    this.linesReserved = Math.max(linesReserved, 1);
    this.printed = new AtomicBoolean(false);
    this.totalMemory = new AtomicLong(0);
    this.usedMemory = new AtomicLong(0);
    this.timeCounter = new AtomicLong(0);
    this.connectionsTotal = new AtomicInteger(0);
    this.idleConnectionsTotal = new AtomicInteger(0);
    this.activeConnectionsTotal = new AtomicInteger(0);
    this.console = console;
    this.title = Optional.ofNullable(title).filter(StringUtils::isNoneBlank).orElse("");
  }

  @Override
  public int line() {
    return line;
  }

  @Override
  public String print() {
    return String.format(
        "%s\t%s\t%s\t%s",
        title,
        String.format(
            "[Memory (Used/Total): %8s/%-8s]",
            Formatters.formatBytes(usedMemory.get()), Formatters.formatBytes(totalMemory.get())),
        String.format(
            "[Connection: Active: %2d Idle: %2s Total: %2d]",
            activeConnectionsTotal.get(), idleConnectionsTotal.get(), connectionsTotal.get()),
        String.format("[Timer: %s]", Formatters.formatSecondsShort(timeCounter.get())));
  }

  /** Update. */
  public void update() {
    totalMemory.set(Runtime.getRuntime().totalMemory());
    usedMemory.set(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
    timeCounter.incrementAndGet();
    console.print(this);
  }

  @Override
  public void stats(final ConnectionPoolStats stats) {
    this.connectionsTotal.set(stats.getConnectionsTotal());
    this.idleConnectionsTotal.set(stats.getIdleConnectionsTotal());
    this.activeConnectionsTotal.set(stats.getActiveConnectionsTotal());
    console.print(this);
  }

  @Override
  public ConsoleColor color() {
    return ConsoleColor.WHITE;
  }

  @Override
  public int linesReserved() {
    return linesReserved;
  }

  @Override
  public boolean isPrinted() {
    return this.printed.get();
  }

  @Override
  public void setPrinted(final boolean printed) {
    this.printed.compareAndSet(false, printed);
  }
}
