package org.bitbucket.abuwandi.snoopy.standalone.output;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntUnaryOperator;
import java.util.stream.Stream;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;

/** The type Rich console. */
public class RichConsole implements Console {

  @Override
  public int newLine() {
    return this.totalLines.incrementAndGet();
  }

  @Override
  public void print(String message) {
    if (StringUtils.isNoneBlank(message)) {
      final String[] lines = StringUtils.split(message, '\n');
      Stream.of(lines).map(this::mapToPrintable).forEach(this::print);
    } else {
      print();
    }
  }

  @Override
  public void print() {
    print(mapToPrintable(""));
  }

  private PrintableMessage mapToPrintable(final String message) {
    final String chompedMessage = StringUtils.chomp(message);
    final int width = getWidth();
    final int linesNeeded =
        Math.max((int) Math.ceil(chompedMessage.length() / (double) (width > 0 ? width : 1)), 1);
    final int lineNumber = this.totalLines.addAndGet(linesNeeded) - (linesNeeded - 1);
    return PrintableMessage.builder()
        .line(lineNumber)
        .message(chompedMessage)
        .linesReserved(linesNeeded)
        .build();
  }

  public int getWidth() {
    final int width = this.width.get();
    return width > 0 ? width : this.width.updateAndGet(this.widthCallable);
  }

  public void print(Printable printable) {
    if (Objects.nonNull(printable) && !output.hasComplete() && !output.hasThrowable()) {
      output.onNext(printable);
    }
  }

  @Override
  public void clearScreen() {
    System.out.print(String.format("%s", CLEAR_SCREEN));
  }

  public boolean shutdown() {
    if (!output.hasComplete() && !output.hasThrowable()) {
      output.onComplete();
      Observable.intervalRange(1, 10, 0, 100, TimeUnit.MILLISECONDS)
          .takeWhile(value -> !disposable.isDisposed())
          .blockingLast(0L);
      return true;
    }
    return false;
  }

  /**
   * Move cursor up.
   *
   * @param lineNumber the line number
   */
  public void moveCursorUp(final int lineNumber) {
    System.out.print(moveCursorUpCode(lineNumber));
  }

  /**
   * Move cursor down.
   *
   * @param lineNumber the line number
   */
  public void moveCursorDown(final int lineNumber) {
    System.out.print(moveCursorDownCode(lineNumber));
  }

  private String moveCursorUpCode(final int lineNumber) {
    return lineNumber < 1 ? "" : String.format("\u001b[%sA", lineNumber);
  }

  private String moveCursorDownCode(final int lineNumber) {
    return lineNumber < 1 ? "" : String.format("\u001b[%sB", lineNumber);
  }

  private String moveCursorCode(final int linesToMove) {
    if (linesToMove >= 0) {
      return moveCursorUpCode(linesToMove);
    }
    return moveCursorDownCode(Math.abs(linesToMove));
  }

  /**
   * Move cursor.
   *
   * @param linesToMove the lines to move
   */
  public void moveCursor(final int linesToMove) {
    System.out.print(moveCursorCode(linesToMove));
  }

  /**
   * Color.
   *
   * @param color the color
   */
  public void color(ConsoleColor color) {
    System.out.print(String.format("%s", color.value()));
  }

  private void clearCurrentLine() {
    System.out.print(String.format("%s", CLEAR_CURRENT_LINE));
  }

  private void handle(final Printable printable) {
    final int cursorLine =
        this.currentLine.getAndSet(printable.line() + printable.linesReserved() - 1);
    if (!printable.isPrinted()) {
      final int linesToMove = printable.line() - cursorLine;
      final String moveCursorDown = moveCursorDownCode(linesToMove);
      final String newLines = StringUtils.repeat('\n', printable.linesReserved());
      final String resetCursor = moveCursorUpCode(printable.linesReserved());
      System.out.printf(
          "%s%s%s%s%s%s",
          moveCursorDown,
          newLines,
          resetCursor,
          CLEAR_CURRENT_LINE,
          printable.color().value(),
          printable.print());
      printable.setPrinted(true);
    } else {
      final int linesToMove = printable.line() - cursorLine;
      final String moveCursorCode =
          linesToMove > 0 ? moveCursorDownCode(linesToMove) : moveCursorUpCode((-1 * linesToMove));
      System.out.printf(
          "%s%s%s%s",
          moveCursorCode, CLEAR_CURRENT_LINE, printable.color().value(), printable.print());
    }
  }

  /** The constant CLEAR_CURRENT_LINE. */
  protected static final String CLEAR_CURRENT_LINE = "\u001b[10000D";

  /** The constant CLEAR_SCREEN. */
  protected static final String CLEAR_SCREEN = "\033[2J\033[H";

  /** The constant SAVE_CURSOR_POSITION. */
  protected static final String SAVE_CURSOR_POSITION = "\u001b[s";

  /** The constant MOVE_CURSOR. */
  protected static final String MOVE_CURSOR = "\u001b[5000;5000H";

  /** The constant REQUEST_CURSOR_POSITION. */
  protected static final String REQUEST_CURSOR_POSITION = "\u001b[6n";

  /** The constant RESTORE_CURSOR_POSITION. */
  protected static final String RESTORE_CURSOR_POSITION = "\u001b[u";

  private final AtomicInteger currentLine;
  private final AtomicInteger totalLines;
  private final PublishSubject<Printable> output;
  private final Disposable disposable;
  private final Scheduler singleThreadScheduler;
  private final AtomicInteger width;
  private final IntUnaryOperator widthCallable;

  /** Instantiates a new Rich console. */
  public RichConsole() {
    this.currentLine = new AtomicInteger(1);
    this.totalLines = new AtomicInteger(0);
    this.singleThreadScheduler = Schedulers.from(Executors.newSingleThreadExecutor());
    this.output = PublishSubject.create();
    this.disposable =
        output
            .toFlowable(BackpressureStrategy.BUFFER)
            .observeOn(singleThreadScheduler)
            .doOnComplete(
                () -> System.out.printf("%s%s", moveCursorDownCode(1), CLEAR_CURRENT_LINE))
            .subscribe(this::handle, throwable -> {});
    this.widthCallable =
        current -> {
          Process process = null;
          BufferedReader reader = null;
          try {
            process = new ProcessBuilder().command("bash", "-c", "tput cols").start();
            reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            process.waitFor();
            final String result = reader.readLine();
            if (StringUtils.isBlank(result) || !StringUtils.isNumeric(result)) {
              return -1;
            }
            return Integer.parseInt(result);
          } catch (Exception exc) {
            return -1;
          } finally {
            IOUtils.close(reader);
            if (Objects.nonNull(process) && process.isAlive()) {
              process.destroy();
            }
          }
        };
    this.width = new AtomicInteger(0);
  }
}
