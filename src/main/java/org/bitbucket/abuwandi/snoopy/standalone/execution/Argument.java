package org.bitbucket.abuwandi.snoopy.standalone.execution;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.Snoopy;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.standalone.output.OutputMode;

/** The enum Argument. */
public enum Argument {
  /** The Authentication. */
  AUTHENTICATION(
      "--authentication",
      "Authentication mode, must be one of [basic, oauth, oauth2, bearer_token]",
      true,
      false,
      false,
      null,
      ArgumentGroup.REQUEST),
  /** The Oauth 2 host. */
  OAUTH2_HOST(
      "--oauth2-host",
      "OAUth2 token issuer host name or ip address",
      true,
      false,
      true,
      null,
      ArgumentGroup.OAUTH2_AUTHENTICATION,
      "--authentication"),
  /** The Oauth 2 username. */
  OAUTH2_USERNAME(
      "--oauth2-username",
      "OAuth2 user name to be passed when requesting a new token",
      true,
      false,
      true,
      null,
      ArgumentGroup.OAUTH2_AUTHENTICATION,
      "--authentication"),
  /** The Oauth 2 password. */
  OAUTH2_PASSWORD(
      "--oauth2-password",
      "OAuth2 password to be passed when requesting a new token",
      true,
      false,
      true,
      null,
      ArgumentGroup.OAUTH2_AUTHENTICATION,
      "--authentication"),
  /** The Oauth 2 grant type. */
  OAUTH2_GRANT_TYPE(
      "--oauth2-grant-type",
      "OAuth2 grant type, must be one of [client_credentials*, password]",
      true,
      false,
      true,
      null,
      ArgumentGroup.OAUTH2_AUTHENTICATION,
      "--authentication"),
  /** The Oauth 2 client id. */
  OAUTH2_CLIENT_ID(
      "--oauth2-client-id",
      "OAuth2 client id to be passed when requesting a new token\nGenerated and provided by a OAuth2 service provider",
      true,
      false,
      true,
      null,
      ArgumentGroup.OAUTH2_AUTHENTICATION,
      "--authentication"),
  /** The Oauth 2 secret. */
  OAUTH2_SECRET(
      "--oauth2-secret",
      "OAUth2 secret to be passed when requesting a new token\nGenerated and provided by a OAuth2 service provider",
      true,
      false,
      true,
      null,
      ArgumentGroup.OAUTH2_AUTHENTICATION,
      "--authentication"),

  /** The Request headers dump. */
  REQUEST_HEADERS_DUMP(
      "--request-headers-dump",
      "Enable request header content output, no value to be specified",
      false,
      false,
      false,
      OutputMode.CONSOLE,
      ArgumentGroup.REQUEST),
  /** The Request body dump. */
  REQUEST_BODY_DUMP(
      "--request-body-dump",
      "Request body content output, must be one of [console*]",
      true,
      false,
      false,
      OutputMode.CONSOLE,
      ArgumentGroup.REQUEST),
  /** The Content type. */
  CONTENT_TYPE(
      "--content-type",
      "Request body content type, must be one of [text, json, binary*]",
      true,
      false,
      false,
      ContentType.TEXT,
      ArgumentGroup.REQUEST),
  /** The Request body. */
  REQUEST_BODY(
      "--body",
      "Request body content, can be either a uri or a double quoted text",
      true,
      false,
      false,
      null,
      ArgumentGroup.REQUEST),

  /** The Response headers dump. */
  RESPONSE_HEADERS_DUMP(
      "--response-headers-dump",
      "Response headers output, must be one of [console*]",
      false,
      false,
      false,
      OutputMode.CONSOLE,
      ArgumentGroup.RESPONSE),
  /** The Response body dump. */
  RESPONSE_BODY_DUMP(
      "--response-body-dump",
      "Response body content output, must be one of [console*, file, or URI]",
      true,
      false,
      false,
      OutputMode.CONSOLE,
      ArgumentGroup.RESPONSE),

  /** The Http method. */
  HTTP_METHOD(
      "--http-method",
      "Request http method, must be one of [get*, post, delete, put, patch, head, options, trace]",
      true,
      false,
      false,
      HttpMethod.GET,
      ArgumentGroup.REQUEST),
  /** Head argument. */
  HEAD("--head", HttpMethod.HEAD.description(), false, false, false, null, ArgumentGroup.REQUEST),
  /** Get argument. */
  GET("--get", HttpMethod.GET.description(), false, false, false, null, ArgumentGroup.REQUEST),
  /** Post argument. */
  POST("--post", HttpMethod.POST.description(), false, false, false, null, ArgumentGroup.REQUEST),
  /** Put argument. */
  PUT("--put", HttpMethod.PUT.description(), false, false, false, null, ArgumentGroup.REQUEST),
  /** Patch argument. */
  PATCH(
      "--patch", HttpMethod.PATCH.description(), false, false, false, null, ArgumentGroup.REQUEST),
  /** Delete argument. */
  DELETE(
      "--delete",
      HttpMethod.DELETE.description(),
      false,
      false,
      false,
      null,
      ArgumentGroup.REQUEST),
  /** Options argument. */
  OPTIONS(
      "--options",
      HttpMethod.OPTIONS.description(),
      false,
      false,
      false,
      null,
      ArgumentGroup.REQUEST),
  /** Trace argument. */
  TRACE(
      "--trace", HttpMethod.TRACE.description(), false, false, false, null, ArgumentGroup.REQUEST),
  /** The Http version. */
  HTTP_VERSION(
      "--http-version",
      "Request http version, must be one of [http1, http11*, http2]",
      true,
      false,
      false,
      "http11",
      ArgumentGroup.REQUEST),
  /** The User agent. */
  USER_AGENT(
      "--user-agent",
      String.format(
          "Sets the request http user agent header, default = %s",
          String.format("SnoopyCLI v%s", Snoopy.VERSION)),
      true,
      false,
      false,
      String.format("SnoopyCLI v%s", Snoopy.VERSION),
      ArgumentGroup.REQUEST),

  /** The Tcp no delay. */
  TCP_NO_DELAY(
      "--tcp-no-delay",
      "Enable or disable tcp option tcp-no-delay, must be either true, false, 1 or 0",
      false,
      false,
      false,
      false,
      ArgumentGroup.TCP),

  /** The Insecure. */
  INSECURE(
      "--insecure",
      "Allow insecure server connections when using SSL",
      false,
      false,
      false,
      false,
      ArgumentGroup.SSL),

  /** The Connections per host. */
  CONNECTIONS_PER_HOST(
      "--connections-per-host",
      "An integer that specifies the maximum size of connections to open\nto a host (Positive values applies connection pooling), default = 5",
      true,
      false,
      false,
      Runtime.getRuntime().availableProcessors(),
      ArgumentGroup.CONNECTION),
  /** The Connection timeout. */
  CONNECTION_TIMEOUT(
      "--connection-timeout",
      "The amount of time in seconds to wait for a connection to be established, default = 5 seconds",
      false,
      false,
      false,
      5,
      ArgumentGroup.CONNECTION),

  /** The Repeat. */
  REPEAT(
      "--repeat",
      "An integer that specifies the number of times to execute the same request, default = 1",
      true,
      false,
      false,
      1,
      ArgumentGroup.GENERAL),
  /** The Send buffer size. */
  SEND_BUFFER_SIZE(
      "--send-buffer-size",
      "Socket send buffer size in bytes, default = 8192 (8 KB)",
      true,
      false,
      false,
      8192L,
      ArgumentGroup.GENERAL),
  /** The Receive buffer size. */
  RECEIVE_BUFFER_SIZE(
      "--receive-buffer-size",
      "Socket receive buffer size in bytes, default = 8192 (8 KB)",
      true,
      false,
      false,
      8192L,
      ArgumentGroup.GENERAL),
  /** The Write buffer low watermark. */
  WRITE_BUFFER_LOW_WATERMARK(
      "--write-buffer-low-watermark",
      "The limit in bytes that indicates when the socket channel\nbecomes in writable state, default = 8192 (8 KB)",
      true,
      false,
      false,
      8192L,
      ArgumentGroup.GENERAL),
  /** The Write buffer high watermark. */
  WRITE_BUFFER_HIGH_WATERMARK(
      "--write-buffer-high-watermark",
      "The limit in bytes that indicates when the socket channel\nbecomes in non writable state, default = 32768 (32 KB)",
      true,
      false,
      false,
      32768L,
      ArgumentGroup.GENERAL),
  /** The Worker threads. */
  WORKER_THREADS(
      "--worker-threads",
      "The number of worker threads responsible for handling IO operations\ndefault value is platform dependent (Number of cpus)",
      true,
      false,
      false,
      Runtime.getRuntime().availableProcessors(),
      ArgumentGroup.GENERAL),
  /** The Execution mode. */
  EXECUTION_MODE(
      "--execution-mode",
      "The strategy to be followed when executing the requests\nmust be one of [sequential*, parallel, accelerated]",
      true,
      false,
      false,
      ExecutionMode.SEQUENTIAL,
      ArgumentGroup.GENERAL),
  /** The Parallel threads. */
  PARALLEL_THREADS(
      "--parallel-execution-threads",
      "The number of worker threads responsible for executing requests in parallel execution mode\ndefault value is platform dependent (Number of cpus)",
      true,
      false,
      false,
      Runtime.getRuntime().availableProcessors(),
      ArgumentGroup.GENERAL,
      "--execution-mode"),

  /** The No audio. */
  NO_AUDIO(
      "--no-audio",
      "Do not download YouTube audio file",
      false,
      false,
      true,
      false,
      ArgumentGroup.YOUTUBE,
      "--execution-mode"),
  /** The No video. */
  NO_VIDEO(
      "--no-video",
      "Do not download YouTube video file",
      false,
      false,
      true,
      false,
      ArgumentGroup.YOUTUBE,
      "--execution-mode");

  private final String value;
  private final String description;
  private final boolean needsValue;
  private final boolean mandatory;
  private final boolean subArgument;
  private final Object defaultValue;
  private final ArgumentGroup group;
  private final String[] dependencies;

  Argument(
      @NonNull final String value,
      @NonNull final String description,
      final boolean needsValue,
      final boolean mandatory,
      final boolean subArgument,
      final Object defaultValue,
      final ArgumentGroup group,
      String... dependencies) {
    this.value = value;
    this.description = description;
    this.needsValue = needsValue;
    this.mandatory = mandatory;
    this.subArgument = subArgument;
    this.defaultValue = defaultValue;
    this.group = group;
    this.dependencies = dependencies;
  }

  /**
   * Needs value boolean.
   *
   * @return the boolean
   */
  public boolean needsValue() {
    return needsValue;
  }

  /**
   * Is mandatory boolean.
   *
   * @return the boolean
   */
  public boolean isMandatory() {
    return mandatory;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * Is sub argument boolean.
   *
   * @return the boolean
   */
  public boolean isSubArgument() {
    return subArgument;
  }

  /**
   * Group argument group.
   *
   * @return the argument group
   */
  public ArgumentGroup group() {
    return group;
  }

  /**
   * Is main argument boolean.
   *
   * @return the boolean
   */
  public boolean isMainArgument() {
    return !subArgument;
  }

  /**
   * Default value object.
   *
   * @return the object
   */
  public Object defaultValue() {
    return defaultValue;
  }

  /**
   * Has default value boolean.
   *
   * @return the boolean
   */
  public boolean hasDefaultValue() {
    return defaultValue != null;
  }

  /**
   * Description string.
   *
   * @return the string
   */
  public String description() {
    return description;
  }

  /**
   * Dependencies list.
   *
   * @return the list
   */
  public List<Argument> dependencies() {
    if (dependencies == null || dependencies.length < 1) {
      return null;
    }
    return Arrays.stream(dependencies)
        .map(Argument::forValue)
        .collect(LinkedList<Argument>::new, LinkedList::add, (arg1, arg2) -> {});
  }

  /**
   * For value argument.
   *
   * @param name the name
   * @return the argument
   */
  public static Argument forValue(final String name) {
    return Optional.ofNullable(name)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(Argument.values())
                    .filter(arg -> arg.value().equals(target))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid argument value")))
        .orElseThrow(() -> new IllegalArgumentException("Invalid argument value"));
  }
}
