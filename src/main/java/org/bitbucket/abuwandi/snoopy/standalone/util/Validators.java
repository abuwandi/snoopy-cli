package org.bitbucket.abuwandi.snoopy.standalone.util;

import java.net.URI;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.authentication.OAuth2;
import org.bitbucket.abuwandi.snoopy.standalone.execution.Argument;
import org.bitbucket.abuwandi.snoopy.standalone.execution.AuthenticationMode;
import org.bitbucket.abuwandi.snoopy.standalone.execution.ExecutionMode;
import org.bitbucket.abuwandi.snoopy.standalone.output.OutputMode;

/** The type Validators. */
public class Validators {

  /**
   * Validate uri uri.
   *
   * @param uri the uri
   * @return the uri
   */
  public static URI validateUri(@NonNull final String uri) {
    if (StringUtils.isBlank(uri)) {
      return null;
    }
    try {
      return new URL(uri).toURI();
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate path path.
   *
   * @param path the path
   * @return the path
   */
  public static Path validatePath(@NonNull final String path) {
    final URI uri = validateUri(path);
    if (Objects.isNull(uri)) {
      return null;
    }
    try {
      return Paths.get(uri);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate integer integer.
   *
   * @param integer the integer
   * @return the integer
   */
  public static Integer validateInteger(@NonNull final String integer) {
    if (!StringUtils.isNumeric(integer)) {
      return null;
    }
    try {
      return Integer.parseInt(integer);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate i long long.
   *
   * @param lng the lng
   * @return the long
   */
  public static Long validateILong(@NonNull final String lng) {
    if (!StringUtils.isNumeric(lng)) {
      return null;
    }
    try {
      return Long.parseLong(lng);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate argument argument.
   *
   * @param arg the arg
   * @return the argument
   */
  public static Argument validateArgument(@NonNull final String arg) {
    if (StringUtils.isBlank(arg)) {
      return null;
    }
    try {
      return Argument.forValue(arg);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate authentication mode authentication mode.
   *
   * @param authMode the auth mode
   * @return the authentication mode
   */
  public static AuthenticationMode validateAuthenticationMode(@NonNull final String authMode) {
    if (StringUtils.isBlank(authMode)) {
      return null;
    }
    try {
      return AuthenticationMode.forValue(authMode);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate output mode output mode.
   *
   * @param outputMode the output mode
   * @return the output mode
   */
  public static OutputMode validateOutputMode(@NonNull final String outputMode) {
    if (StringUtils.isBlank(outputMode)) {
      return null;
    }
    try {
      return OutputMode.forValue(outputMode);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate request body output mode.
   *
   * @param requestBody the request body
   * @return the output mode
   */
  public static OutputMode validateRequestBody(@NonNull final String requestBody) {
    if (StringUtils.isBlank(requestBody)) {
      return null;
    }
    try {
      return OutputMode.forValue(requestBody);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate grant type o auth 2 . grant type.
   *
   * @param grantType the grant type
   * @return the o auth 2 . grant type
   */
  public static OAuth2.GrantType validateGrantType(@NonNull final String grantType) {
    if (StringUtils.isBlank(grantType)) {
      return null;
    }
    try {
      return OAuth2.GrantType.valueForName(grantType);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate http method http method.
   *
   * @param httpMethod the http method
   * @return the http method
   */
  public static HttpMethod validateHttpMethod(@NonNull final String httpMethod) {
    if (StringUtils.isBlank(httpMethod)) {
      return null;
    }
    try {
      return HttpMethod.forValue(httpMethod);
    } catch (Exception exc) {
      return null;
    }
  }

  /**
   * Validate execution mode execution mode.
   *
   * @param executionMode the execution mode
   * @return the execution mode
   */
  public static ExecutionMode validateExecutionMode(@NonNull final String executionMode) {
    if (StringUtils.isBlank(executionMode)) {
      return null;
    }
    try {
      return ExecutionMode.forValue(executionMode);
    } catch (Exception exc) {
      return null;
    }
  }
}
