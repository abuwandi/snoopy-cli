package org.bitbucket.abuwandi.snoopy.standalone.util;

import io.netty.handler.codec.http.HttpHeaderValues;
import org.apache.commons.lang3.StringUtils;

/** The type Http utils. */
public class HttpUtils {

  private HttpUtils() {
    throw new AssertionError("HttpUtils cannot be instantiated");
  }

  /**
   * Is text boolean.
   *
   * @param contentType the content type
   * @return the boolean
   */
  public static boolean isText(final CharSequence contentType) {
    return StringUtils.isNoneBlank(contentType)
        && StringUtils.equalsAnyIgnoreCase(
            contentType,
            HttpHeaderValues.APPLICATION_JSON,
            HttpHeaderValues.APPLICATION_XML,
            HttpHeaderValues.APPLICATION_XHTML,
            HttpHeaderValues.APPLICATION_X_WWW_FORM_URLENCODED,
            HttpHeaderValues.TEXT_CSS,
            HttpHeaderValues.TEXT_HTML,
            HttpHeaderValues.TEXT_PLAIN,
            HttpHeaderValues.TEXT_EVENT_STREAM);
  }
}
