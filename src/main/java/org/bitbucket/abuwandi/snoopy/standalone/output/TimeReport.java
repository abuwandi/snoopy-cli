package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.util.concurrent.atomic.AtomicBoolean;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import org.bitbucket.abuwandi.snoopy.standalone.util.Formatters;

/** The type Time report. */
@Getter
public class TimeReport implements Printable {

  private final long connectionTime;
  private final long requestTime;
  private final long responseTime;

  @Getter(AccessLevel.NONE)
  private final int linesReserved;

  @Getter(AccessLevel.NONE)
  private final int line;

  @Getter(AccessLevel.NONE)
  private final AtomicBoolean printed;

  /**
   * Instantiates a new Time report.
   *
   * @param line the line
   * @param connectionTime the connection time
   * @param responseTime the response time
   * @param requestTime the request time
   * @param linesReserved the lines reserved
   */
  @Builder
  public TimeReport(
      final int line,
      final long connectionTime,
      final long responseTime,
      final long requestTime,
      final int linesReserved) {
    this.line = line;
    this.connectionTime = connectionTime;
    this.requestTime = requestTime;
    this.responseTime = responseTime;
    this.linesReserved = Math.max(linesReserved, 1);
    this.printed = new AtomicBoolean(false);
  }

  @Override
  public int line() {
    return line;
  }

  @Override
  public String print() {
    return String.format(
        "Total Time: %s, [Connection Time: %s, Request Time: %s, Response Time: %s]",
        Formatters.formatMillisShort(connectionTime + requestTime + responseTime),
        Formatters.formatMillisShort(connectionTime),
        Formatters.formatMillisShort(requestTime),
        Formatters.formatMillisShort(responseTime));
  }

  @Override
  public ConsoleColor color() {
    return ConsoleColor.DEFAULT;
  }

  @Override
  public int linesReserved() {
    return linesReserved;
  }

  @Override
  public boolean isPrinted() {
    return this.printed.get();
  }

  @Override
  public void setPrinted(final boolean printed) {
    this.printed.compareAndSet(false, printed);
  }
}
