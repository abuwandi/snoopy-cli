package org.bitbucket.abuwandi.snoopy.standalone.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.TimeUnit;
import org.bitbucket.abuwandi.snoopy.standalone.output.ConsoleProgress;

/** The type Io utils. */
public class IOUtils {

  /** The constant DEFAULT_BUFFER_SIZE. */
  public static final int DEFAULT_BUFFER_SIZE = 8192;

  private IOUtils() {
    throw new AssertionError("IOUtils cannot be instantiated");
  }

  /**
   * Copy long.
   *
   * @param src the src
   * @param dst the dst
   * @param progress the progress
   * @return the long
   * @throws IOException the io exception
   */
  public static long copy(
      final InputStream src, final OutputStream dst, final ConsoleProgress progress)
      throws IOException {
    return copy(Channels.newChannel(src), Channels.newChannel(dst), progress);
  }

  /**
   * Copy long.
   *
   * @param src the src
   * @param dst the dst
   * @param progress the progress
   * @return the long
   * @throws IOException the io exception
   */
  public static long copy(
      final ReadableByteChannel src, final WritableByteChannel dst, final ConsoleProgress progress)
      throws IOException {
    return copy(src, dst, ByteBuffer.allocateDirect(DEFAULT_BUFFER_SIZE), progress);
  }

  /**
   * Copy long.
   *
   * @param src the src
   * @param dst the dst
   * @param buffer the buffer
   * @param progress the progress
   * @return the long
   * @throws IOException the io exception
   */
  public static long copy(
      ReadableByteChannel src,
      WritableByteChannel dst,
      final ByteBuffer buffer,
      final ConsoleProgress progress)
      throws IOException {

    long bytesRead;
    long totalBytesRead = 0;

    long time = 0;
    long bytes = 0;

    while ((bytesRead = src.read(buffer)) > -1) {
      totalBytesRead += bytesRead;
      buffer.flip();
      final int beforeAvailableBytes = buffer.remaining();
      final long beforeTime = System.nanoTime();
      dst.write(buffer);
      final long afterTime = System.nanoTime();
      final int afterAvailableBytes = buffer.remaining();
      final long writtenBytes = beforeAvailableBytes - afterAvailableBytes;
      time += (afterTime - beforeTime);
      bytes += writtenBytes;
      if (TimeUnit.NANOSECONDS.toSeconds(time) >= 1) {
        time = 0;
        progress.updateThroughput(bytes);
        bytes = 0;
      }
      progress.updateValue(writtenBytes);
      buffer.compact();
    }

    buffer.flip();

    while (buffer.hasRemaining()) {
      final int beforeAvailableBytes = buffer.remaining();
      final long beforeTime = System.nanoTime();
      dst.write(buffer);
      final long afterTime = System.nanoTime();
      final int afterAvailableBytes = buffer.remaining();
      final long writtenBytes = beforeAvailableBytes - afterAvailableBytes;
      time += (afterTime - beforeTime);
      bytes += writtenBytes;
      if (TimeUnit.NANOSECONDS.toSeconds(time) >= 1) {
        time = 0;
        progress.updateThroughput(bytes);
        bytes = 0;
      }
      progress.updateValue(writtenBytes);
    }

    return totalBytesRead;
  }
}
