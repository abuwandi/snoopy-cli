package org.bitbucket.abuwandi.snoopy.standalone.execution;

import java.net.URI;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Builder;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.Snoopy;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;
import org.bitbucket.abuwandi.snoopy.standalone.util.Formatters;

/** The type Help execution. */
public class HelpExecution extends HttpExecution {

  /**
   * Instantiates a new Help execution.
   *
   * @param config the config
   * @param prefetchAccessTokens the prefetch access tokens
   * @param authentication the authentication
   * @param console the console
   * @param uri the uri
   * @param httpMethod the http method
   * @param responseOutputUri the response output uri
   * @param headers the headers
   */
  @Builder
  public HelpExecution(
      final SnoopyConfig config,
      final boolean prefetchAccessTokens,
      final Authentication authentication,
      final Console console,
      final URI uri,
      final HttpMethod httpMethod,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers) {
    super(
        config,
        prefetchAccessTokens,
        authentication,
        console,
        uri,
        httpMethod,
        responseOutputUri,
        headers);
  }

  @Override
  protected void preExecute() {}

  @Override
  protected ExecutionReport doExecute(URI uri, URI outputUri) {
    console.print(" \nUsage: snoopy-cli [options...] <url>");
    final int width = console.getWidth();
    final String separator =
        width > 0 ? StringUtils.leftPad("-", width, '-') : Formatters.SEPARATOR;
    Stream.of(Argument.values()).collect(Collectors.groupingBy(Argument::group)).entrySet().stream()
        .sorted(Comparator.comparingInt(arg -> arg.getKey().value()))
        .collect(
            LinkedList<String>::new,
            (collector, entry) -> {
              collector.add(Formatters.format(entry.getKey(), " "));
              entry.getValue().stream().map(Formatters::format).forEach(collector::add);
            },
            (c1, c2) -> {})
        .forEach(console::print);
    console.print(String.format(" \nSnoopy CLI v%s", Snoopy.VERSION));
    return null;
  }
}
