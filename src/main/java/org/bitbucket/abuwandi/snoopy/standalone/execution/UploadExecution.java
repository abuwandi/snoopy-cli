package org.bitbucket.abuwandi.snoopy.standalone.execution;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Builder;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.Status;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.HttpPostRequestBuilder;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ConsoleProgress;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;
import org.bitbucket.abuwandi.snoopy.standalone.output.ResourcesUtilizationReport;
import org.bitbucket.abuwandi.snoopy.standalone.util.Formatters;
import org.bitbucket.abuwandi.snoopy.standalone.util.ProgressStatus;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;

/** The type Upload execution. */
public class UploadExecution extends HttpExecution {

  private final int repeat;
  private final URI requestBodyUri;
  private final Scheduler executeScheduler;

  /**
   * Instantiates a new Upload execution.
   *
   * @param config the config
   * @param authentication the authentication
   * @param prefetchAccessTokens the prefetch access tokens
   * @param console the console
   * @param uri the uri
   * @param responseOutputUri the response output uri
   * @param headers the headers
   * @param requestBodyUri the request body uri
   * @param httpMethod the http method
   * @param repeat the repeat
   */
  @Builder
  public UploadExecution(
      final SnoopyConfig config,
      final Authentication authentication,
      final boolean prefetchAccessTokens,
      final Console console,
      final URI uri,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers,
      final URI requestBodyUri,
      final HttpMethod httpMethod,
      final int repeat) {
    super(
        config,
        prefetchAccessTokens,
        authentication,
        console,
        uri,
        httpMethod,
        responseOutputUri,
        headers);
    this.repeat = repeat;
    this.requestBodyUri = requestBodyUri;
    this.executeScheduler = Schedulers.from(Executors.newSingleThreadExecutor());
  }

  @Override
  protected void preExecute() {
    final int sendBufferSize = (int) Math.max(config.getSendBufferSize(), ByteUnit.MIB.toBytes(2));
    config.setSendBufferSize(sendBufferSize);
    super.preExecute();
  }

  @Override
  public ExecutionReport doExecute(final URI uri, final URI outputUri) {
    try {
      final HttpPostRequestBuilder requestBuilder =
          createPostRequestBuilder(uri).headers(headers).body(requestBodyUri);
      final Path path = Paths.get(requestBodyUri);
      final int count =
          Flowable.range(1, repeat)
              .flatMapMaybe(
                  counter -> {
                    final long contentLength =
                        Optional.ofNullable(requestBuilder.contentLength())
                            .filter(StringUtils::isNotBlank)
                            .filter(StringUtils::isNumeric)
                            .map(CharSequence::toString)
                            .map(Long::parseLong)
                            .orElse(0L);
                    console.print(
                        String.format(
                            "Uploading file: %s size: %s",
                            path.getFileName(), Formatters.formatBytes(contentLength)));
                    final ConsoleProgress progress =
                        newProgressBar(String.format("Upload %s", counter), contentLength, 1);
                    progress.updateValue(0);
                    console.print(" ");
                    final ResourcesUtilizationReport resourcesUtilizationReport =
                        ResourcesUtilizationReport.builder()
                            .title("Upload memory monitor:")
                            .console(console)
                            .line(console.newLine())
                            .build();
                    installConnectionPoolStatsListener(resourcesUtilizationReport);
                    final Disposable disposable =
                        Observable.intervalRange(0, Integer.MAX_VALUE, 0, 1, TimeUnit.SECONDS)
                            .map(
                                value -> {
                                  resourcesUtilizationReport.update();
                                  return value;
                                })
                            .doFinally(
                                () ->
                                    uninstallConnectionPoolStatsListener(
                                        resourcesUtilizationReport))
                            .subscribe();
                    return requestBuilder
                        .progressCallback(progress::updateValue)
                        .statusCallback(
                            newStatus ->
                                progress.updateStatus(
                                    Optional.of(newStatus)
                                        .filter(status -> status != Status.IDLE)
                                        .map(Status::name)
                                        .map(ProgressStatus::valueOf)
                                        .orElse(ProgressStatus.ACTIVE)))
                        .execute()
                        .flatMapMaybe(response -> handle(response, outputUri))
                        .doFinally(disposable::dispose);
                  })
              .subscribeOn(executeScheduler)
              .map(disposal -> 1)
              .collect(AtomicInteger::new, AtomicInteger::addAndGet)
              .map(AtomicInteger::get)
              .blockingGet();
      return null;
    } catch (Exception exc) {
      throw new ExecutionException(exc);
    }
  }

  @Override
  public void shutdown(final Throwable throwable) {
    super.shutdown(throwable);
  }
}
