package org.bitbucket.abuwandi.snoopy.standalone.execution;

import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;

/** The interface Execution. */
public interface Execution {

  /**
   * Execute execution report.
   *
   * @return the execution report
   */
  ExecutionReport execute();

  /**
   * Shutdown.
   *
   * @param throwable the throwable
   */
  void shutdown(Throwable throwable);
}
