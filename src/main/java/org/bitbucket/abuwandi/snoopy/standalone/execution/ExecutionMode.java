package org.bitbucket.abuwandi.snoopy.standalone.execution;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/** The enum Execution mode. */
public enum ExecutionMode {
  /** The Sequential. */
  SEQUENTIAL(
      "sequential",
      "Requests will be executed in a sequential fashion using a single thread and one request at atime"),
  /** The Parallel. */
  PARALLEL(
      "parallel",
      "Requests will be executed in a parallel fashion using multiple threads and many requests at atime",
      "--parallel-execution-threads"),
  /** The Accelerated. */
  ACCELERATED(
      "accelerated",
      "A request will be broken into multiple requests, each will have its own connection and response, all responses once completed will be merged in as a one final response",
      "--parallel-execution-threads"),
  /** The Youtube. */
  YOUTUBE(
      "youtube",
      "Downloads youtube videos",
      "--parallel-execution-threads",
      "--no-audio",
      "--no-video"),
  /** The Upload. */
  UPLOAD("upload", "Upload files", "--body");

  private final String value;
  private final String description;
  private final String[] dependencies;

  ExecutionMode(final String value, final String description, String... dependencies) {
    this.value = value;
    this.description = description;
    this.dependencies = dependencies;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * Description string.
   *
   * @return the string
   */
  public String description() {
    return description;
  }

  /**
   * Dependencies list.
   *
   * @return the list
   */
  public List<Argument> dependencies() {
    if (dependencies == null || dependencies.length < 1) {
      return null;
    }
    return Arrays.stream(dependencies)
        .map(Argument::forValue)
        .collect(LinkedList<Argument>::new, LinkedList::add, (arg1, arg2) -> {});
  }

  /**
   * For value execution mode.
   *
   * @param value the value
   * @return the execution mode
   */
  public static ExecutionMode forValue(final String value) {
    return Optional.ofNullable(value)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(ExecutionMode.values())
                    .filter(arg -> arg.value().equals(target))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid execution mode")))
        .orElseThrow(() -> new IllegalArgumentException("Invalid execution mode"));
  }
}
