package org.bitbucket.abuwandi.snoopy.standalone.execution;

/** The enum Content type. */
public enum ContentType {
  /** Json content type. */
  JSON("json"),
  /** Binary content type. */
  BINARY("binary"),
  /** Text content type. */
  TEXT("text");

  private final String value;

  ContentType(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }
}
