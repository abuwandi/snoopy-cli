package org.bitbucket.abuwandi.snoopy.standalone.output;

import io.netty.buffer.ByteBuf;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.util.Disposal;
import org.bitbucket.abuwandi.snoopy.util.IOUtils;
import org.bitbucket.abuwandi.snoopy.util.Progress;

/** The type Byte buf console disposal. */
public class ByteBufConsoleDisposal implements Disposal<ByteBuf> {

  private final Progress progress;
  private final Charset charset;
  private final Console console;
  private final AtomicLong totalBytesDisposed;

  /**
   * Instantiates a new Byte buf console disposal.
   *
   * @param progress the progress
   * @param charset the charset
   * @param console the console
   */
  @Builder
  public ByteBufConsoleDisposal(
      final Progress progress, final Charset charset, final Console console) {
    this.progress = progress;
    this.charset = charset;
    this.console = console;
    this.totalBytesDisposed = new AtomicLong(0);
  }

  @Override
  public void dispose(final ByteBuf buffer) {
    final long readableBytes = buffer.readableBytes();
    try {
      console.print(buffer.toString(charset));
      if (Objects.nonNull(progress)) {
        progress.updateValue(readableBytes);
      }
      totalBytesDisposed.addAndGet(readableBytes);
    } finally {
      IOUtils.release(buffer);
    }
  }

  @Override
  public Long finish() {
    return totalBytesDisposed.get();
  }

  @Override
  public void close() {}
}
