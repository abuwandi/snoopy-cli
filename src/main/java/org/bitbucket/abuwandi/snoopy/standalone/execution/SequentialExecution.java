package org.bitbucket.abuwandi.snoopy.standalone.execution;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestBuilder;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;

/** The type Sequential execution. */
public class SequentialExecution extends HttpExecution {

  private final int repeat;
  private final Scheduler executeScheduler;

  /**
   * Instantiates a new Sequential execution.
   *
   * @param config the config
   * @param authentication the authentication
   * @param prefetchAccessTokens the prefetch access tokens
   * @param console the console
   * @param uri the uri
   * @param responseOutputUri the response output uri
   * @param headers the headers
   * @param httpMethod the http method
   * @param repeat the repeat
   */
  @Builder
  public SequentialExecution(
      final SnoopyConfig config,
      final Authentication authentication,
      final boolean prefetchAccessTokens,
      final Console console,
      final URI uri,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers,
      final HttpMethod httpMethod,
      final int repeat) {
    super(
        config,
        prefetchAccessTokens,
        authentication,
        console,
        uri,
        httpMethod,
        responseOutputUri,
        headers);
    this.repeat = repeat;
    this.executeScheduler = Schedulers.from(Executors.newSingleThreadExecutor());
  }

  @Override
  public ExecutionReport doExecute(final URI uri, final URI outputUri) {
    final HttpRequestBuilder requestBuilder = createRequestBuilder(httpMethod, uri);
    try {
      final int count =
          Flowable.range(1, repeat)
              .flatMapMaybe(
                  counter ->
                      requestBuilder
                          .headers(headers)
                          .execute()
                          .flatMapMaybe(response -> handle(response, outputUri)))
              .subscribeOn(executeScheduler)
              .map(disposal -> 1)
              .collect(AtomicInteger::new, AtomicInteger::addAndGet)
              .map(AtomicInteger::get)
              .blockingGet();
      return null;
    } catch (Exception exc) {
      throw new ExecutionException(exc);
    }
  }

  @Override
  public void shutdown(final Throwable throwable) {
    super.shutdown(throwable);
  }
}
