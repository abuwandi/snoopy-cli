package org.bitbucket.abuwandi.snoopy.standalone.execution;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import java.net.URI;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpResponse;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;

/** The type You tube execution. */
public class YouTubeExecution extends AcceleratedGetExecution {

  @Override
  protected void preExecute() {
    final CharSequence userAgent =
        Optional.ofNullable(headers.get(HttpHeaderNames.USER_AGENT))
            .map(list -> list.stream().findFirst().orElse(LEGIT_USER_AGENT))
            .orElse(LEGIT_USER_AGENT);
    final int receiveBufferSize =
        (int) Math.max(config.getReceiveBufferSize(), ByteUnit.MIB.toBytes(2));
    final int connectionsPerHost = Math.max(config.getMaximumHostConnections(), executorCount);
    config.setMaximumHostConnections(connectionsPerHost);
    config.setReceiveBufferSize(receiveBufferSize);
    headers.put(HttpHeaderNames.USER_AGENT, Collections.singletonList(userAgent));
    super.preExecute();
  }

  @Override
  public ExecutionReport doExecute(final URI uri, final URI outputUri) {
    console.print(String.format("YouTube video url detected %s", uri));
    console.print("Nice!! :-) let the fun begin...");
    try {
      final PlayerResponse playerResponse = getPlayerResponse(uri);
      console.print("Extracting video urls completed");
      final Format[] formats =
          ArrayUtils.addAll(
              playerResponse.getStreamingData().getAdaptiveFormats(),
              playerResponse.getStreamingData().getFormats());
      if (ArrayUtils.isEmpty(formats)) {
        throw new ExecutionException("Media formats not found!");
      }
      console.print(
          String.format(
              "Found a video titled: %s with %d formats ",
              playerResponse.getVideoDetails().getTitle(), formats.length));
      console.print("Extracting formats...");
      headers.put(HttpHeaderNames.CONNECTION, Collections.singletonList(HttpHeaderValues.CLOSE));
      headers.put(HttpHeaderNames.ACCEPT, Collections.singletonList("*/*"));
      headers.put(HttpHeaderNames.ACCEPT_ENCODING, Collections.singletonList("gzip, deflate, br"));
      headers.put(HttpHeaderNames.ACCEPT_LANGUAGE, Collections.singletonList("en-US,en;q=0.5"));
      headers.put("DNT", Collections.singletonList("1"));
      headers.put(HttpHeaderNames.ORIGIN, Collections.singletonList("https://www.youtube.com"));
      headers.put(HttpHeaderNames.REFERER, Collections.singletonList("https://www.youtube.com/"));
      final URI audioOutputUri;
      final URI videoOutputUri;
      final ExecutionReport audioExecutionReport;
      final ExecutionReport videoExecutionReport;
      if (!noAudio) {
        final Format audioFormat = findAudioFormat(formats);
        if (Objects.nonNull(audioFormat)) {
          final URI audioUri = new URL(audioFormat.getUrl()).toURI();
          audioOutputUri = validateResponseUri(outputUri, playerResponse, audioFormat);
          audioExecutionReport = super.doExecute(audioUri, audioOutputUri);
        } else {
          audioOutputUri = null;
          audioExecutionReport = ExecutionReport.builder().build();
        }
      } else {
        console.print("Skipping audio download.");
        audioOutputUri = null;
        audioExecutionReport = null;
      }
      if (!noVideo) {
        final Format videoFormat = findVideoFormat(formats);
        final URI videoUri = new URL(videoFormat.getUrl()).toURI();
        videoOutputUri = validateResponseUri(outputUri, playerResponse, videoFormat);
        videoExecutionReport = super.doExecute(videoUri, videoOutputUri);
      } else {
        console.print("Skipping video download.");
        videoOutputUri = null;
        videoExecutionReport = null;
      }
      if (Objects.nonNull(audioOutputUri) && Objects.nonNull(videoOutputUri)) {
        final String commandStr =
            String.format(
                "ffmpeg -i %s -i %s -acodec copy -vcodec copy %s",
                Paths.get(audioOutputUri).toString(),
                Paths.get(videoOutputUri).toString(),
                Paths.get(
                        Paths.get(videoOutputUri).getParent().toString(),
                        StringUtils.replace(
                            Paths.get(videoOutputUri).getFileName().toString(), "-video", ""))
                    .toString());
        console.print("Run the command below to merge audio and video files:");
        console.print(commandStr);
      }
      return videoExecutionReport;
    } catch (ExecutionException exc) {
      throw exc;
    } catch (Exception exc) {
      throw new ExecutionException("Execution failed", exc);
    }
  }

  @Override
  public void shutdown(final Throwable throwable) {
    super.shutdown(throwable);
  }

  private PlayerResponse getPlayerResponse(final URI uri) {
    return createRequestBuilder(HttpMethod.GET, uri)
        .closeConnection()
        .execute()
        .flatMap(SnoopyHttpResponse::bodyAsString)
        .doOnSubscribe(disposable -> console.print("Extracting video urls..."))
        .map(
            body -> {
              final Matcher matcher =
                  //                  Pattern.compile(";ytplayer\\.config =
                  // (\\{.*?\\});").matcher(body);
                  Pattern.compile("var ytInitialPlayerResponse = (\\{.*?\\});").matcher(body);
              if (matcher.find()) {
                final String match = matcher.group(1);
                return match;
              }
              throw new ExecutionException("Video player config not found!");
            })
        .map(
            configStr -> {
              final ObjectMapper mapper = new ObjectMapper();
              mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
              return mapper.readValue(configStr, PlayerResponse.class);
            })
        .blockingGet();
  }

  private Format findAudioFormat(final Format[] formats) {
    final List<Format> audioFormats =
        Stream.of(formats)
            .filter(f -> StringUtils.isNoneBlank(f.getUrl()))
            .filter(f -> StringUtils.startsWithAny(f.getMimeType(), "audio/"))
            .filter(
                f ->
                    StringUtils.equalsAny(
                        f.getAudioQuality(),
                        "AUDIO_QUALITY_HIGH",
                        "AUDIO_QUALITY_MEDIUM",
                        "AUDIO_QUALITY_LOW"))
            .collect(Collectors.toList());
    console.print(String.format("Found %d audio formats", audioFormats.size()));
    return audioFormats.stream()
        .findFirst()
        .map(
            fmt -> {
              console.print(
                  String.format(
                      "Best audio quality format type: %s quality: %s",
                      fmt.getMimeType(), Optional.ofNullable(fmt.getAudioQuality()).orElse("n/a")));
              return fmt;
            })
        .orElse(null);
  }

  private Format findVideoFormat(final Format[] formats) {
    final List<Format> videoFormats =
        Stream.of(formats)
            .filter(f -> StringUtils.isNoneBlank(f.getUrl()))
            .filter(f -> StringUtils.startsWithAny(f.getMimeType(), "video/"))
            .filter(
                f ->
                    StringUtils.equalsAny(
                        f.getQualityLabel(),
                        "2160p60",
                        "2160p",
                        "1440p60",
                        "1440p",
                        "1080p60",
                        "1080p",
                        "720p60",
                        "720p",
                        "480p",
                        "360p",
                        "320p",
                        "240p",
                        "144p"))
            .collect(Collectors.toList());
    console.print(String.format("Found %d video formats", videoFormats.size()));
    return videoFormats.stream()
        .findFirst()
        .map(
            fmt -> {
              console.print(
                  String.format(
                      "Best video quality format type: %s quality: %s",
                      fmt.getMimeType(), Optional.ofNullable(fmt.getQualityLabel()).orElse("n/a")));
              return fmt;
            })
        .orElseThrow(() -> new ExecutionException("video not found"));
  }

  private URI validateResponseUri(
      final URI uri, final PlayerResponse playerResponse, final Format format) {
    if (Objects.nonNull(uri)) {
      return uri;
    }
    return Paths.get(
            System.getProperty("user.dir"),
            String.format("%s%s", extractFileName(playerResponse), extractExtension(format)))
        .toUri();
  }

  private String extractFileName(final PlayerResponse playerResponse) {
    final String originalTitle = playerResponse.getVideoDetails().getTitle();
    return originalTitle.replaceAll("[^a-zA-Z0-9-_]+", "").replaceAll("\\s+", "_");
  }

  private String extractExtension(final Format format) {
    final Matcher matcher =
        Pattern.compile("(video|audio)/([a-zA-Z0-9]+)\\b").matcher(format.getMimeType());
    if (!matcher.find()) {
      return "";
    }
    return "-" + matcher.group(1) + "." + matcher.group(2);
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @ToString
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private static class Format {
    private int itag;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private String url;

    @Setter(AccessLevel.NONE)
    private String cipher;

    private String mimeType;
    private long bitrate;
    private long contentLength;
    private String qualityLabel;
    private String audioQuality;
    private int fps;

    /**
     * Gets url.
     *
     * @return the url
     */
    public String getUrl() {
      return StringUtils.isNotBlank(url) ? url : cipher;
    }

    /**
     * Sets url.
     *
     * @param url the url
     */
    public void setUrl(String url) {
      if (StringUtils.isBlank(url)) {
        return;
      }
      try {
        this.url = extractUrl(URLDecoder.decode(url, StandardCharsets.UTF_8.toString()));
      } catch (Exception exc) {
        throw new ExecutionException(exc);
      }
    }

    private String extractUrl(final String url) {
      final Matcher matcher = Pattern.compile("https?://[^\\s]+").matcher(url);
      if (matcher.find()) {
        return matcher.group();
      }
      return "";
    }

    /**
     * Sets cipher.
     *
     * @param cipher the cipher
     */
    public void setCipher(String cipher) {
      if (StringUtils.isBlank(cipher) || cipher.length() <= "url=".length()) {
        return;
      }
      try {
        this.cipher = extractUrl(URLDecoder.decode(cipher, StandardCharsets.UTF_8.toString()));
      } catch (Exception exc) {
        throw new ExecutionException(exc);
      }
    }
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private static class VideoDetails {
    private String title;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private static class StreamingData {
    private String expiresInSeconds;
    private Format[] formats;
    private Format[] adaptiveFormats;
  }

  @Getter
  @Setter
  @NoArgsConstructor
  @AllArgsConstructor
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private static class PlayerResponse {
    private StreamingData streamingData;
    private VideoDetails videoDetails;
  }

  private static final String LEGIT_USER_AGENT =
      "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0";
  private final boolean noAudio;
  private final boolean noVideo;

  /**
   * Instantiates a new You tube execution.
   *
   * @param config the config
   * @param console the console
   * @param uri the uri
   * @param responseOutputUri the response output uri
   * @param headers the headers
   * @param executorCount the executor count
   * @param noAudio the no audio
   * @param noVideo the no video
   */
  @Builder(builderMethodName = "youtubeExecutionBuilder")
  public YouTubeExecution(
      final SnoopyConfig config,
      final Console console,
      final URI uri,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers,
      final int executorCount,
      final boolean noAudio,
      final boolean noVideo) {
    super(config, null, false, console, uri, responseOutputUri, headers, executorCount);
    this.noAudio = noAudio;
    this.noVideo = noVideo;
  }
}
