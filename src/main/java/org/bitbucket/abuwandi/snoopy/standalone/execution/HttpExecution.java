package org.bitbucket.abuwandi.snoopy.standalone.execution;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.buffer.ByteBuf;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.bitbucket.abuwandi.snoopy.client.Snoopy;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.authentication.AccessTokenCache;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.bootstrap.ConnectionPoolStatsListener;
import org.bitbucket.abuwandi.snoopy.client.http.HttpGetRequestBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.HttpPostRequestBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestBuilder;
import org.bitbucket.abuwandi.snoopy.client.http.SnoopyHttpResponse;
import org.bitbucket.abuwandi.snoopy.client.http.authentication.OAuth2;
import org.bitbucket.abuwandi.snoopy.client.http.authentication.OAuth2AccessToken;
import org.bitbucket.abuwandi.snoopy.client.http.util.Extractors;
import org.bitbucket.abuwandi.snoopy.standalone.output.ByteBufConsoleDisposal;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ConsoleProgress;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;
import org.bitbucket.abuwandi.snoopy.standalone.util.HttpUtils;
import org.bitbucket.abuwandi.snoopy.standalone.util.ProgressStatus;
import org.bitbucket.abuwandi.snoopy.util.Disposal;

/** The type Http execution. */
public abstract class HttpExecution implements Execution {

  @Override
  public final ExecutionReport execute() {
    preExecute();
    final ExecutionReport executionReport = doExecute(uri, responseOutputUri);
    console.print(executionReport);
    postExecute();
    return executionReport;
  }

  /** Pre execute. */
  protected void preExecute() {
    config.setConnectionPoolStatsListener(
        stats -> {
          final ConnectionPoolStatsListener statsListener = this.connectionPoolStatsListener.get();
          if (Objects.nonNull(statsListener)) {
            statsListener.stats(stats);
          }
        });
    snoopy = Snoopy.builder().config(config).build();
  }

  /** Post execute. */
  protected void postExecute() {}

  /**
   * Do execute execution report.
   *
   * @param uri the uri
   * @param outputUri the output uri
   * @return the execution report
   */
  protected abstract ExecutionReport doExecute(final URI uri, final URI outputUri);

  @Override
  public void shutdown(final Throwable throwable) {
    if (Objects.nonNull(throwable)) {
      console.print(
          String.format(
              "Oops! That's embarrassing!!! Something went wrong%nError: %s",
              ExceptionUtils.getRootCauseMessage(throwable)));
      throwable.printStackTrace();
    }
    console.print("Exiting...");
    console.shutdown();
    if (Objects.nonNull(snoopy)) {
      snoopy.shutdown();
    }
  }

  /**
   * Handle maybe.
   *
   * @param response the response
   * @param responseBodyOutput the response body output
   * @return the maybe
   */
  protected Maybe<?> handle(
      @NonNull final SnoopyHttpResponse response, final URI responseBodyOutput) {
    console.print(Extractors.dumpInfo(response.request()));
    console.print(Extractors.dumpHeaders(response.request()));
    console.print(Extractors.dumpStatus(response));
    console.print(Extractors.dumpHeaders(response));
    final CharSequence contentType = response.contentType();
    if (response.hasBody() && HttpUtils.isText(response.contentType())) {
      if (StringUtils.equalsIgnoreCase(contentType, HttpHeaderValues.APPLICATION_JSON)) {
        return Maybe.fromCallable(
                () ->
                    new Disposal<String>() {
                      @Override
                      public void dispose(final String output) {
                        final ObjectMapper mapper = new ObjectMapper();
                        try {
                          final JsonNode jsonNode = mapper.reader().readTree(output);
                          console.print(
                              mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode));
                        } catch (Exception exc) {
                          throw new IllegalStateException(exc);
                        }
                      }

                      @Override
                      public void close() {}
                    })
            .map(
                disposal -> {
                  disposal.dispose(response.bodyAsString().blockingGet());
                  return disposal;
                });
      }
      return response
          .bodyAs(ByteBuf.class)
          .collect(
              () ->
                  ByteBufConsoleDisposal.builder()
                      .console(console)
                      .charset(response.charset())
                      .build(),
              Disposal::dispose)
          .map(Disposal::finish)
          .toMaybe();
    } else if (response.hasBody()) {
      final Path path =
          Optional.ofNullable(responseOutputUri)
              .map(Paths::get)
              .orElseGet(
                  () ->
                      Paths.get(
                          System.getProperty("user.home"),
                          Optional.ofNullable(response.filename())
                              .map(CharSequence::toString)
                              .orElseGet(
                                  () ->
                                      String.format(
                                          "%s%s.bin", "SnoopyCLI_Response_", System.nanoTime()))));
      return Extractors.saveBodyToFile(response, path).toMaybe();
    }

    return Maybe.empty();
  }

  /**
   * Create request builder http request builder.
   *
   * @param httpMethod the http method
   * @param uri the uri
   * @return the http request builder
   */
  protected HttpRequestBuilder createRequestBuilder(
      @NonNull final HttpMethod httpMethod, @NonNull final URI uri) {
    final HttpRequestBuilder builder;
    switch (httpMethod) {
      case HEAD:
        builder =
            snoopy
                .head(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
        break;
      case POST:
        builder =
            snoopy
                .post(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
        break;
      case DELETE:
        builder =
            snoopy
                .delete(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
        break;
      case PUT:
        builder =
            snoopy
                .put(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
        break;
      case PATCH:
        builder =
            snoopy
                .patch(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
        break;
      case OPTIONS:
        builder =
            snoopy
                .options(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
        break;
      case GET:
      default:
        builder =
            snoopy
                .get(uri)
                .followRedirects(true)
                .failIfNotSuccessfulResponse(true)
                .connectionRetry(3);
    }

    if (authentication instanceof OAuth2) {
      builder
          .authentication(authentication)
          .tokenProvider(
              AccessTokenCache.builder()
                  .tokenProvider(
                      authentication -> {
                        try {
                          console.print("Authenticating request...");
                          final OAuth2 oauth2 = (OAuth2) authentication;
                          final OAuth2AccessToken token =
                              snoopy
                                  .post(oauth2.getUri())
                                  .failIfNotSuccessfulResponse(true)
                                  .followRedirects(true)
                                  .connectionRetry(3)
                                  .basic(oauth2.getClientId(), oauth2.getSecret())
                                  .consumeAs(OAuth2AccessToken.class);
                          console.print("Got authentication token");
                          return Single.just(token);
                        } catch (Throwable throwable) {
                          return Single.error(throwable);
                        }
                      })
                  .build());
    }
    return builder;
  }

  /**
   * Create get request builder http get request builder.
   *
   * @param uri the uri
   * @return the http get request builder
   */
  protected HttpGetRequestBuilder createGetRequestBuilder(@NonNull final URI uri) {
    return snoopy.get(uri).followRedirects(true).failIfNotSuccessfulResponse(true);
  }

  /**
   * Create post request builder http post request builder.
   *
   * @param uri the uri
   * @return the http post request builder
   */
  protected HttpPostRequestBuilder createPostRequestBuilder(@NonNull final URI uri) {
    return snoopy.post(uri).followRedirects(true).failIfNotSuccessfulResponse(true);
  }

  /**
   * New progress bar console progress.
   *
   * @param title the title
   * @param maxValue the max value
   * @return the console progress
   */
  protected ConsoleProgress newProgressBar(final String title, final long maxValue) {
    return newProgressBar(title, maxValue, 10);
  }

  /**
   * New progress bar console progress.
   *
   * @param title the title
   * @param maxValue the max value
   * @param refreshEach the refresh each
   * @return the console progress
   */
  protected ConsoleProgress newProgressBar(
      final String title, final long maxValue, final int refreshEach) {
    return ConsoleProgress.builder()
        .console(console)
        .line(console.newLine())
        .title(title)
        .status(ProgressStatus.PENDING)
        .maxValue(maxValue)
        .refreshEach(refreshEach)
        .build();
  }

  /**
   * Install connection pool stats listener boolean.
   *
   * @param statsListener the stats listener
   * @return the boolean
   */
  protected boolean installConnectionPoolStatsListener(
      final ConnectionPoolStatsListener statsListener) {
    return connectionPoolStatsListener.compareAndSet(null, statsListener);
  }

  /**
   * Uninstall connection pool stats listener boolean.
   *
   * @param statsListener the stats listener
   * @return the boolean
   */
  protected boolean uninstallConnectionPoolStatsListener(
      final ConnectionPoolStatsListener statsListener) {
    return connectionPoolStatsListener.compareAndSet(statsListener, null);
  }

  /** The Config. */
  protected final SnoopyConfig config;

  /** The Prefetch access tokens. */
  protected final boolean prefetchAccessTokens;

  /** The Authentication. */
  protected final Authentication authentication;

  /** The Console. */
  protected final Console console;

  /** The Headers. */
  protected final Map<CharSequence, List<CharSequence>> headers;

  private final URI uri;

  /** The Http method. */
  protected final HttpMethod httpMethod;

  private final URI responseOutputUri;

  private Snoopy snoopy;

  private final AtomicReference<ConnectionPoolStatsListener> connectionPoolStatsListener;

  /**
   * Instantiates a new Http execution.
   *
   * @param config the config
   * @param prefetchAccessTokens the prefetch access tokens
   * @param authentication the authentication
   * @param console the console
   * @param uri the uri
   * @param httpMethod the http method
   * @param responseOutputUri the response output uri
   * @param headers the headers
   */
  public HttpExecution(
      final SnoopyConfig config,
      final boolean prefetchAccessTokens,
      final Authentication authentication,
      final Console console,
      final URI uri,
      final HttpMethod httpMethod,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers) {
    this.config = config;
    this.prefetchAccessTokens = prefetchAccessTokens;
    this.authentication = authentication;
    this.console = console;
    this.uri = uri;
    this.responseOutputUri = responseOutputUri;
    this.headers = headers;
    this.httpMethod = httpMethod;
    this.connectionPoolStatsListener = new AtomicReference<>(null);
  }
}
