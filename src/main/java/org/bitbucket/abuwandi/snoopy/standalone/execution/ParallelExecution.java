package org.bitbucket.abuwandi.snoopy.standalone.execution;

import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.client.SnoopyConfig;
import org.bitbucket.abuwandi.snoopy.client.authentication.Authentication;
import org.bitbucket.abuwandi.snoopy.client.http.HttpMethod;
import org.bitbucket.abuwandi.snoopy.client.http.HttpRequestBuilder;
import org.bitbucket.abuwandi.snoopy.standalone.output.Console;
import org.bitbucket.abuwandi.snoopy.standalone.output.ExecutionReport;

/** The type Parallel execution. */
public class ParallelExecution extends HttpExecution {

  private final int repeat;
  private final int executorCount;
  private final Scheduler executeScheduler;
  private final Scheduler observerScheduler;

  /**
   * Instantiates a new Parallel execution.
   *
   * @param config the config
   * @param authentication the authentication
   * @param prefetchAccessTokens the prefetch access tokens
   * @param console the console
   * @param uri the uri
   * @param responseOutputUri the response output uri
   * @param headers the headers
   * @param httpMethod the http method
   * @param repeat the repeat
   * @param executorCount the executor count
   */
  @Builder
  public ParallelExecution(
      final SnoopyConfig config,
      final Authentication authentication,
      final boolean prefetchAccessTokens,
      final Console console,
      final URI uri,
      final URI responseOutputUri,
      final Map<CharSequence, List<CharSequence>> headers,
      final HttpMethod httpMethod,
      final int repeat,
      final int executorCount) {
    super(
        config,
        prefetchAccessTokens,
        authentication,
        console,
        uri,
        httpMethod,
        responseOutputUri,
        headers);
    this.repeat = repeat;
    this.executorCount = executorCount;
    this.executeScheduler = Schedulers.from(Executors.newFixedThreadPool(executorCount));
    this.observerScheduler = Schedulers.from(Executors.newSingleThreadExecutor());
  }

  @Override
  public ExecutionReport doExecute(final URI uri, final URI outputUri) {
    final HttpRequestBuilder requestBuilder = createRequestBuilder(httpMethod, uri);
    try {
      Flowable.range(1, repeat)
          .flatMapMaybe(
              counter ->
                  requestBuilder
                      .headers(headers)
                      .build()
                      .execute()
                      .flatMapMaybe(response -> handle(response, outputUri))
                      .subscribeOn(executeScheduler))
          .observeOn(observerScheduler)
          .blockingSubscribe(output -> {}, throwable -> {});
      return null;
    } catch (Exception exc) {
      throw new ExecutionException(exc);
    }
  }

  @Override
  public void shutdown(final Throwable throwable) {
    super.shutdown(throwable);
  }
}
