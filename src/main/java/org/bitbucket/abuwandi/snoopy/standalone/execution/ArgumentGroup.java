package org.bitbucket.abuwandi.snoopy.standalone.execution;

/** The enum Argument group. */
public enum ArgumentGroup {
  /** The Request. */
  REQUEST("Request Options:", 1),
  /** The Response. */
  RESPONSE("Response Options:", 2),
  /** The Basic authentication. */
  BASIC_AUTHENTICATION("Basic Authentication Options:", 3),
  /** The Oauth 2 authentication. */
  OAUTH2_AUTHENTICATION("OAuth2 Authentication Options:", 4),
  /** The Tcp. */
  TCP("Tcp Options:", 5),
  /** The Connection. */
  CONNECTION("Connection Options:", 6),
  /** The General. */
  GENERAL("General Options:", 7),
  /** The Youtube. */
  YOUTUBE("YouTube Options:", 8),
  /** The Ssl. */
  SSL("Ssl Options:", 9);

  private final int value;
  private final String displayName;

  ArgumentGroup(final String displayName, final int value) {
    this.displayName = displayName;
    this.value = value;
  }

  /**
   * Display name string.
   *
   * @return the string
   */
  public String displayName() {
    return displayName;
  }

  /**
   * Value int.
   *
   * @return the int
   */
  public int value() {
    return value;
  }
}
