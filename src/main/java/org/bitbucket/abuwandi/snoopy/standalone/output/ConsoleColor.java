package org.bitbucket.abuwandi.snoopy.standalone.output;

/** The enum Console color. */
public enum ConsoleColor {
  /** Red console color. */
  RED("\u001b[31m"),
  /** Default console color. */
  DEFAULT("\u001b[0m"),
  /** Black console color. */
  BLACK("\u001b[30m"),
  /** Green console color. */
  GREEN("\u001b[32m"),
  /** Yellow console color. */
  YELLOW(" \u001b[33m"),
  /** Blue console color. */
  BLUE("\u001b[34m"),
  /** Magenta console color. */
  MAGENTA("\u001b[35m"),
  /** Cyan console color. */
  CYAN("\u001b[36m"),
  /** White console color. */
  WHITE("\u001b[37m");

  private final String value;

  ConsoleColor(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }
}
