package org.bitbucket.abuwandi.snoopy.standalone.output;

import java.nio.charset.Charset;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import lombok.Builder;
import org.bitbucket.abuwandi.snoopy.util.Disposal;
import org.bitbucket.abuwandi.snoopy.util.Progress;

/** The type Byte buf console disposal. */
public class ByteArrayConsoleDisposal implements Disposal<byte[]> {

  private final Progress progress;
  private final Charset charset;
  private final Console console;
  private final AtomicLong totalBytesDisposed;

  /**
   * Instantiates a new Byte buf console disposal.
   *
   * @param progress the progress
   * @param charset the charset
   * @param console the console
   */
  @Builder
  public ByteArrayConsoleDisposal(
      final Progress progress, final Charset charset, final Console console) {
    this.progress = progress;
    this.charset = charset;
    this.console = console;
    this.totalBytesDisposed = new AtomicLong(0);
  }

  @Override
  public void dispose(final byte[] buffer) {
    final long readableBytes = buffer.length;
    if (readableBytes < 1) {
      return;
    }
    console.print(new String(buffer, charset));
    if (Objects.nonNull(progress)) {
      progress.updateValue(readableBytes);
    }
    totalBytesDisposed.addAndGet(readableBytes);
  }

  @Override
  public Long finish() {
    return totalBytesDisposed.get();
  }

  @Override
  public void close() {}
}
