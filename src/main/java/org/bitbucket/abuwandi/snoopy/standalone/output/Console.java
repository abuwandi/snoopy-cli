package org.bitbucket.abuwandi.snoopy.standalone.output;

/** The interface Console. */
public interface Console {

  /**
   * New line int.
   *
   * @return the int
   */
  int newLine();

  /**
   * Print.
   *
   * @param printable the printable
   */
  void print(Printable printable);

  /**
   * Print.
   *
   * @param message the message
   */
  void print(String message);

  /** Print. */
  void print();

  /**
   * Shutdown boolean.
   *
   * @return the boolean
   */
  boolean shutdown();

  /** Clear screen. */
  void clearScreen();

  /**
   * Gets width.
   *
   * @return the width
   */
  int getWidth();
}
