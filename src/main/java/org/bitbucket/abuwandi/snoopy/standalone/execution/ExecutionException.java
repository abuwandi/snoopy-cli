package org.bitbucket.abuwandi.snoopy.standalone.execution;

/** The type Execution exception. */
public class ExecutionException extends RuntimeException {

  /**
   * Instantiates a new Execution exception.
   *
   * @param message the message
   */
  public ExecutionException(String message) {
    super(message);
  }

  /**
   * Instantiates a new Execution exception.
   *
   * @param message the message
   * @param cause the cause
   */
  public ExecutionException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Instantiates a new Execution exception.
   *
   * @param cause the cause
   */
  public ExecutionException(Throwable cause) {
    super(cause);
  }
}
