package org.bitbucket.abuwandi.snoopy.standalone.execution;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;

/** The enum Authentication mode. */
public enum AuthenticationMode {
  /** The Basic. */
  BASIC("basic", "Basic Authentication"),
  /** The Oauth. */
  OAUTH("oauth", "Open Standard Authentication version 1.0"),
  /** The Oauth 2. */
  OAUTH2(
      "oauth2",
      "Open Standard Authentication version 2.0",
      "--oauth2-host",
      "--oauth2-grant-type",
      "--oauth2-client-id",
      "--oauth2-secret"),
  /** The Bearer token. */
  BEARER_TOKEN("bearer_token", "Token Based Authentication");

  private final String value;
  private final String description;
  private final String[] dependencies;

  AuthenticationMode(final String value, final String description, final String... dependencies) {
    this.value = value;
    this.description = description;
    this.dependencies = dependencies;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }

  /**
   * Description string.
   *
   * @return the string
   */
  public String description() {
    return description;
  }

  /**
   * Dependencies list.
   *
   * @return the list
   */
  public List<Argument> dependencies() {
    if (dependencies == null || dependencies.length < 1) {
      return null;
    }
    return Arrays.stream(dependencies)
        .map(Argument::forValue)
        .collect(LinkedList<Argument>::new, LinkedList::add, (arg1, arg2) -> {});
  }

  /**
   * For value authentication mode.
   *
   * @param name the name
   * @return the authentication mode
   */
  public static AuthenticationMode forValue(String name) {
    return Optional.ofNullable(name)
        .filter(StringUtils::isNoneBlank)
        .map(
            target ->
                Arrays.stream(AuthenticationMode.values())
                    .filter(arg -> arg.value.equals(target))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("Invalid argument name")))
        .orElseThrow(() -> new IllegalArgumentException("Invalid argument name"));
  }
}
