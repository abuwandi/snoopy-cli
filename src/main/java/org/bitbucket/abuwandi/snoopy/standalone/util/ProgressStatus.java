package org.bitbucket.abuwandi.snoopy.standalone.util;

/** The enum Progress status. */
public enum ProgressStatus {
  /** Pending progress status. */
  PENDING("Pending"),
  /** Idle progress status. */
  IDLE("Idle"),
  /** Active progress status. */
  ACTIVE("Active"),
  /** Complete progress status. */
  COMPLETE("Complete"),
  /** Failed progress status. */
  FAILED("Failed");

  private final String value;

  ProgressStatus(final String value) {
    this.value = value;
  }

  /**
   * Value string.
   *
   * @return the string
   */
  public String value() {
    return value;
  }
}
