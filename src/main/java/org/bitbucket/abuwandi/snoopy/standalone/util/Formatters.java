package org.bitbucket.abuwandi.snoopy.standalone.util;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.bitbucket.abuwandi.snoopy.standalone.execution.Argument;
import org.bitbucket.abuwandi.snoopy.standalone.execution.ArgumentGroup;
import org.bitbucket.abuwandi.snoopy.standalone.output.ConsoleProgress;
import org.bitbucket.abuwandi.snoopy.util.ByteUnit;

/** The type Formatters. */
public class Formatters {

  /**
   * Format nanos full string.
   *
   * @param nanos the nanos
   * @return the string
   */
  public static String formatNanosFull(long nanos) {
    return String.format(
        "Hours:%02d Minutes:%02d Seconds:%02d Millis:%03d Micros:%03d Nanos:%03d",
        TimeUnit.NANOSECONDS.toHours(nanos),
        TimeUnit.NANOSECONDS.toMinutes(nanos) % TimeUnit.HOURS.toMinutes(1),
        TimeUnit.NANOSECONDS.toSeconds(nanos) % TimeUnit.MINUTES.toSeconds(1),
        TimeUnit.NANOSECONDS.toMillis(nanos) % TimeUnit.SECONDS.toMillis(1),
        TimeUnit.NANOSECONDS.toMicros(nanos) % TimeUnit.MILLISECONDS.toMicros(1),
        nanos % TimeUnit.MICROSECONDS.toNanos(1));
  }

  /**
   * Format nanos short string.
   *
   * @param nanos the nanos
   * @return the string
   */
  public static String formatNanosShort(long nanos) {
    return String.format(
        "%02d:%02d:%02d.%03d:%03d:%03d",
        TimeUnit.NANOSECONDS.toHours(nanos),
        TimeUnit.NANOSECONDS.toMinutes(nanos) % TimeUnit.HOURS.toMinutes(1),
        TimeUnit.NANOSECONDS.toSeconds(nanos) % TimeUnit.MINUTES.toSeconds(1),
        TimeUnit.NANOSECONDS.toMillis(nanos) % TimeUnit.SECONDS.toMillis(1),
        TimeUnit.NANOSECONDS.toMicros(nanos) % TimeUnit.MILLISECONDS.toMicros(1),
        nanos % TimeUnit.MICROSECONDS.toNanos(1));
  }

  /**
   * Format millis short string.
   *
   * @param nanos the nanos
   * @return the string
   */
  public static String formatMillisShort(long nanos) {
    final long hours = TimeUnit.NANOSECONDS.toHours(nanos);
    if (hours > 0) {
      return String.format(
          "%02dh %02dm %02ds %03dms",
          TimeUnit.NANOSECONDS.toHours(nanos),
          TimeUnit.NANOSECONDS.toMinutes(nanos) % TimeUnit.HOURS.toMinutes(1),
          TimeUnit.NANOSECONDS.toSeconds(nanos) % TimeUnit.MINUTES.toSeconds(1),
          TimeUnit.NANOSECONDS.toMillis(nanos) % TimeUnit.SECONDS.toMillis(1));
    }
    final long minutes = TimeUnit.NANOSECONDS.toMinutes(nanos) % TimeUnit.HOURS.toMinutes(1);
    if (minutes > 0) {
      return String.format(
          "%02dm %02ds %03dms",
          TimeUnit.NANOSECONDS.toMinutes(nanos) % TimeUnit.HOURS.toMinutes(1),
          TimeUnit.NANOSECONDS.toSeconds(nanos) % TimeUnit.MINUTES.toSeconds(1),
          TimeUnit.NANOSECONDS.toMillis(nanos) % TimeUnit.SECONDS.toMillis(1));
    }
    final long seconds = TimeUnit.NANOSECONDS.toSeconds(nanos) % TimeUnit.MINUTES.toSeconds(1);
    if (seconds > 0) {
      return String.format(
          "%02ds %03dms",
          TimeUnit.NANOSECONDS.toSeconds(nanos) % TimeUnit.MINUTES.toSeconds(1),
          TimeUnit.NANOSECONDS.toMillis(nanos) % TimeUnit.SECONDS.toMillis(1));
    }
    return String.format(
        "%03dms", TimeUnit.NANOSECONDS.toMillis(nanos) % TimeUnit.SECONDS.toMillis(1));
  }

  /**
   * Format seconds short string.
   *
   * @param seconds the seconds
   * @return the string
   */
  public static String formatSecondsShort(final long seconds) {
    if (seconds < 0) {
      return "";
    }
    final long hours = TimeUnit.SECONDS.toHours(seconds);
    if (hours > 0) {
      return String.format(
          "%02dh %02dm %02ds",
          TimeUnit.SECONDS.toHours(seconds),
          TimeUnit.SECONDS.toMinutes(seconds) % TimeUnit.HOURS.toMinutes(1),
          seconds % TimeUnit.MINUTES.toSeconds(1));
    }
    final long minutes = TimeUnit.SECONDS.toMinutes(seconds) % TimeUnit.HOURS.toMinutes(1);
    if (minutes > 0) {
      return String.format(
          "%02dm %02ds",
          TimeUnit.SECONDS.toMinutes(seconds) % TimeUnit.HOURS.toMinutes(1),
          seconds % TimeUnit.MINUTES.toSeconds(1));
    }
    return String.format("%02ds", seconds % TimeUnit.MINUTES.toSeconds(1));
  }

  /**
   * Format string.
   *
   * @param progress the progress
   * @return the string
   */
  public static String format(@NonNull final ConsoleProgress progress) {
    final float progressValue = progress.progress();
    final int progressbarValue = (int) progressValue / 2;
    final String progressbar =
        FULL_PROGRESS_BAR.substring(0, progressbarValue)
            + (progressbarValue < FULL_PROGRESS_BAR.length() ? ">" : "");
    final String formattedMaxValue = formatBytes(progress.maxValue());
    final String formattedCurrentValue = formatBytes(progress.currentValue());
    final String formattedSpeed = formatTrafficSpeed(progress.throughput());
    final long estimatedRemainingTime =
        progress.throughput() < 1 ? -1 : progress.remaining() / progress.throughput();
    final String formattedRemainingTime =
        String.format(
            "ETA %-11s",
            Optional.of(formatSecondsShort(estimatedRemainingTime))
                .filter(StringUtils::isNoneBlank)
                .orElse("forever"));
    final String formattedProgressValue =
        String.format("%9s/%-9s", formattedCurrentValue, formattedMaxValue);
    return String.format(
        "%-20s %6.2f%%\t[%-50s] %-54s ",
        progress.title(),
        progressValue,
        progressbar,
        String.format(
            "%19s %-11s %16s %-8s",
            formattedProgressValue, formattedSpeed, formattedRemainingTime, progress.status()));
  }

  /**
   * Format string.
   *
   * @param argumentGroup the argument group
   * @param separator the separator
   * @return the string
   */
  public static String format(@NonNull ArgumentGroup argumentGroup, final String separator) {
    return String.format(" \n \\ %s\n%s", argumentGroup.displayName(), " |");
  }

  /**
   * Format string.
   *
   * @param argument the argument
   * @return the string
   */
  public static String format(@NonNull Argument argument) {
    final String[] lines = StringUtils.split(argument.description(), '\n');
    final StringBuilder builder =
        new StringBuilder(String.format(" |-\u25CF%30s\t%s\n", argument.value(), lines[0]));
    return Stream.of(lines)
        .skip(1)
        .collect(
            () -> builder,
            (bldr, line) -> bldr.append(String.format(" |  %-30s\t%s\n", "", line)),
            (bldr1, bldr2) -> {})
        .toString();
  }

  /** The constant PROGRESS_FORMAT. */
  public static final String PROGRESS_FORMAT = "%s%s\t%-6.2f%% [%-50s] %s\t";
  /** The constant FULL_PROGRESS_BAR. */
  public static final String FULL_PROGRESS_BAR = StringUtils.leftPad("=", 50, '=');
  /** The constant SEPARATOR. */
  public static final String SEPARATOR = StringUtils.leftPad("-", 50, '-');

  private Formatters() {
    throw new AssertionError("Formatters cannot be instantiated");
  }

  /**
   * Format bytes string.
   *
   * @param throughput the throughput
   * @return the string
   */
  public static String formatBytes(final long throughput) {
    int value = (int) ByteUnit.BYTE.toKiB(throughput);
    if (value < 1) {
      return String.format("%s", String.format("%dB", throughput));
    }
    value = (int) ByteUnit.BYTE.toMiB(throughput);
    if (value < 1) {
      return String.format("%s", String.format("%.1fKiB", ByteUnit.BYTE.toKiB(throughput)));
    }
    value = (int) ByteUnit.BYTE.toGiB(throughput);
    if (value < 1) {
      return String.format("%s", String.format("%.1fMiB", ByteUnit.BYTE.toMiB(throughput)));
    }
    return String.format("%s", String.format("%.1fGiB", ByteUnit.BYTE.toGiB(throughput)));
  }

  /**
   * Format traffic speed string.
   *
   * @param throughput the throughput
   * @return the string
   */
  public static String formatTrafficSpeed(final long throughput) {
    int value = (int) ByteUnit.BYTE.toKiB(throughput);
    if (value < 1) {
      return String.format("%8sB/s", String.format("%s", throughput));
    }
    value = (int) ByteUnit.BYTE.toMiB(throughput);
    if (value < 1) {
      return String.format("%6sKiB/s", String.format("%.1f", ByteUnit.BYTE.toKiB(throughput)));
    }
    value = (int) ByteUnit.BYTE.toGiB(throughput);
    if (value < 1) {
      return String.format("%6sMiB/s", String.format("%.1f", ByteUnit.BYTE.toMiB(throughput)));
    }
    return String.format("%6sGiB/s", String.format("%.1f", ByteUnit.BYTE.toGiB(throughput)));
  }
}
