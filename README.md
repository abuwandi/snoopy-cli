Snoopy command line interface
=============================

See the [project website][snoopy-cli] for documentation and APIs.

snoopy-cli is the [Snoopy][snoopy] command line interface, a simple program that allows to send commands to http 
servers, and read the replies sent by the server, directly from the terminal.

It requires the java 8+, and it is not platform specific. It works on Linux, MacOS, Unix and Windows.

Usage
---------------------------------------
```
snoopy-cli [options...] <url>
```

Download Accelerator
----------------------------------------
snoopy-cli can be used as a download accelerator, it increases download speeds by up to 5 times, resume downloads. 
Comprehensive error recovery and resume capability will restart broken or interrupted downloads due to lost connections, or network problems. 
It reuses available connections without additional connect and login stages to achieve best acceleration performance.
```
snoopy-cli https://speed.hetzner.de/1GB.bin
```
### Options ###
By default it uses current system resources efficiently, while allowing custom options to be passed 
```
--receive-buffer-size         	Socket receive buffer size in bytes, default = 8192 (8 KB)
--parallel-execution-threads  	The number of worker threads responsible for executing requests in parallel execution mode
```

YouTube Downloader
----------------------------------------
snoopy-cli can be used  to download videos from YouTube.com.

```
snoopy-cli https://www.youtube.com/watch?v=r3SoJOloQO0
```
### Options ###
```
--no-audio                    	Do not download YouTube audio file
--no-video                    	Do not download YouTube video file
--receive-buffer-size         	Socket receive buffer size in bytes, default = 8192 (8 KB)
--parallel-execution-threads  	The number of worker threads responsible for executing requests in parallel execution mode
```

Testing tool
----------------------------------------
snoopy-cli be used to test performance both on static and dynamic resources, Web dynamic applications.
It can be used to simulate a heavy load on a server, group of servers, network or object to test its strength or to analyze overall performance under different load types.
### Options ###
```
+ Request Options:
--------------------------------------------------
--authentication              	Authentication mode, must be one of [basic, oauth, oauth2, bearer_token]
--request-headers-dump        	Enable request header content output, no value to be specified
--request-body-dump           	Request body content output, must be one of [console*]
--content-type                	Request body content type, must be one of [text, json, binary*]
--body                        	Request body content, can be a uri or quoted text
--http-method                 	Request http method, must be one of [get*, post, delete, put, patch, head, options, trace]
--head                        	Asks for a response of a resource but without the response body
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD
--get                         	Requests a representation of a resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET
--post                        	Creates a new resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
--put                         	Creates a new resource or replaces a representation of a resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
--patch                       	Applies partial modifications to a resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH
--delete                      	Deletes a resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE
--options                     	Describes the communication options for a resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS
--trace                       	Performs a message loop-back test along the path of a resource
                              	for more info please visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/TRACE
--http-version                	Request http version, must be one of [http1, http11*, http2]
--user-agent                  	Sets the request http user agent header, default = Snoopyv0.5

+ Response Options:
--------------------------------------------------
--response-headers-dump       	Response headers output, must be one of [console*]
--response-body-dump          	Response body content output, must be one of [console*, file, or URI]

+ OAuth2 Authentication Options:
--------------------------------------------------
--oauth2-host                 	OAUth2 token issuer host name or ip address
--oauth2-username             	OAuth2 user name to be passed when requesting a new token
--oauth2-password             	OAuth2 password to be passed when requesting a new token
--oauth2-grant-type           	OAuth2 grant type, must be one of [client_credentials*, password]
--oauth2-client-id            	OAuth2 client id to be passed when requesting a new token
                              	Generated and provided by a OAuth2 service provider
--oauth2-secret               	OAUth2 secret to be passed when requesting a new token
                              	Generated and provided by a OAuth2 service provider

+ Tcp Options:
--------------------------------------------------
--tcp-no-delay                	Enable or disable tcp option tcp-no-delay, must be either true, false, 1 or 0

+ Connection Options:
--------------------------------------------------
--connections-per-host        	An integer that specifies the maximum size of connections to open
                              	to a host (Positive values applies connection pooling), default = 5
--connection-timeout          	The amount of time in seconds to wait for a connection to be established, default = 5 seconds

+ General Options:
--------------------------------------------------
--repeat                      	An integer that specifies the number of times to execute the same request, default = 1
--send-buffer-size            	Socket send buffer size in bytes, default = 8192 (8 KB)
--receive-buffer-size         	Socket receive buffer size in bytes, default = 8192 (8 KB)
--write-buffer-low-watermark  	The limit in bytes that indicates when the socket channel
                              	becomes in writable state, default = 8192 (8 KB)
--write-buffer-high-watermark 	The limit in bytes that indicates when the socket channel
                              	becomes in non writable state, default = 32768 (32 KB)
--worker-threads              	The number of worker threads responsible for handling IO operations
                              	default value is platform dependent (Number of cpus)
--execution-mode              	The strategy to be followed when executing the requests
                              	must be one of [sequential*, parallel, accelerated]
--parallel-execution-threads  	The number of worker threads responsible for executing requests in parallel execution mode
                              	default value is platform dependent (Number of cpus)

+ YouTube Options:
--------------------------------------------------
--no-audio                    	Do not download YouTube audio file
--no-video                    	Do not download YouTube video file
```

And more...
----------------------------------------

 [snoopy-cli]: https://bitbucket.org/abuwandi/snoopy-cli
 [snoopy]: https://bitbucket.org/abuwandi/snoopy/
 [changelog]: https://bitbucket.org/abuwandi/snoopy-cli